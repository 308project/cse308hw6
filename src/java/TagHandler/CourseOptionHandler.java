/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package TagHandler;

import cse308.Section;
import cse308.Student;
import java.util.ArrayList;
import java.util.Calendar;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author ShawnCruz
 */
public class CourseOptionHandler extends SimpleTagSupport {

    private String userEmail;
    
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();
        
         try {
            
            JspFragment f = getJspBody();
            
            String fEmail = userEmail;
            SessionFactory sFactory =
                    new Configuration().configure().buildSessionFactory();
            Session openSession = sFactory.openSession();
            
            String hqlSections = "from Student S, Section C"
                    + " where S.schoolCode = C.schoolCode"
                    + " and S.emailAddress = '" + userEmail + "'";
            Query sectionsQuery = openSession.createQuery(hqlSections);
            ArrayList<Object[]> sectionsList = 
                    (ArrayList<Object[]>)sectionsQuery.list();
            openSession.close();
            
            Student student;
            Section section;
            String courseName, courseCode, sectionNumber, uniqueCourse, instructorName;
            int multiSelectIndex = 0;
            ArrayList<Section> courseOptions = new ArrayList<>();
            //Displaying the result of the DB in the Request Tag on the admin's page
            for (Object[] sectionItem : sectionsList) {
                student = (Student)sectionItem[0];
                section = (Section)sectionItem[1];
                
                courseName = section.getCourseName();
                courseCode = section.getCourseCode();
                instructorName =section.getProfessorName();
     
                //ONLY ADDS COURSE AVAILABLE CURRENT YEAR
                 if (section.getYear() == Calendar.getInstance().get(Calendar.YEAR)) {
                     courseOptions.add(section);
                     sectionNumber = "-0" + section.getSectionNumber();
                     uniqueCourse = courseCode + sectionNumber;
                     String desiredCourseElement = uniqueCourse;
                     getJspContext().setAttribute("desiredCourseElement", desiredCourseElement);
                     getJspContext().setAttribute("multiSelectIndex", multiSelectIndex++);
                     getJspContext().setAttribute("instructorName", instructorName);
                     if (f != null) {
                         f.invoke(out);
                     }
                 }
             }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in CourseOptionHandler tag", ex);
        }
    }
    
    public void setUserEmail(String email) {
        this.userEmail = email;
    }
}
