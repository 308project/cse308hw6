package TagHandler;

import cse308.Student;
import cse308.User;
import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Request extends SimpleTagSupport {
    
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();
        
        try {
            JspFragment f = getJspBody();
            Query queryStudent;
            SessionFactory sFactory = 
                    new Configuration().configure().buildSessionFactory();
            Session openSession = sFactory.openSession();
            String sql_studentQuery = "from Student s, User u where "
                    + "(u.emailAddress = s.emailAddress) AND "
                    + "(u.accountApproved = '0' )";
            
            queryStudent = openSession.createQuery(sql_studentQuery);
            
            ArrayList<Object[]> studentList = new ArrayList<>();
            studentList = (ArrayList<Object[]>) queryStudent.list();
            openSession.close();
            
            User studentRequest;
            Student student;
            
            //Displaying the result of the DB in the Request Tag on the admin's page
            // for (int i = 0; i < studentList.size(); i++) {
            for (Object[] tableItem : studentList) {
                student = (Student) tableItem[0];
                studentRequest = (User) tableItem[1];
                getJspContext().setAttribute("fName", studentRequest.getFirstName());
                getJspContext().setAttribute("lName", studentRequest.getLastName());
                getJspContext().setAttribute("email", studentRequest.getEmailAddress());
                System.out.println(studentRequest.getFirstName());
                
                if (f != null) {
                    f.invoke(out);
                }
            }
        } catch (java.io.IOException ex) {
            throw new JspException("Error in request tag", ex);
        }
    }
}