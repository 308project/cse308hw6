package TagHandler;

import cse308.Friends;
import cse308.PlannerMethods;
import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class FriendsTagHandler extends SimpleTagSupport {
    
    private String userEmail;
    
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();
        
        try {
            
            JspFragment f = getJspBody();
            
            String fEmail = userEmail;
            SessionFactory sFactory =
                    new Configuration().configure().buildSessionFactory();
            Session openSession = sFactory.openSession();
            
            String hqlFriends = "from Friends F where (F.userEmail = "
                    + "'"+fEmail+"') OR ( F.friendEmail='"+fEmail+"')";
            Query friendQuery = openSession.createQuery(hqlFriends);
            ArrayList<Friends> friendsList = 
                    (ArrayList<Friends>)friendQuery.list();
            openSession.close();
            
            //Displaying the result of the DB in the Request Tag on the admin's page
            for (Friends friend : friendsList) {
                String eMail = friend.getFriendEmail();
                String myEmail = friend.getUserEmail();
                if(!eMail.equals(userEmail))
                {
                    String str = "<li><a>" + 
                            PlannerMethods.getUserList().get(eMail) +"<span onclick=\"unfriend('"+eMail+"')\" style = \"float: right;\" class=\"glyphicon glyphicon-remove\"></span></a></li>";
                    getJspContext().setAttribute("friend", str);
                }
                else
                    if(!myEmail.equals(userEmail))
                    {
                        String str = "<li><a>"+ 
                                PlannerMethods.getUserList().get(myEmail) +"<span onclick=\"unfriend('"+myEmail+"')\" style = \"float: right;\" class=\"glyphicon glyphicon-remove delFriend\"></span></a></li>";
                        getJspContext().setAttribute("friend", str);
                        
                    }
                if (f != null) {
                    f.invoke(out);
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in FriendsTagHandler tag", ex);
        }
    }
    
    public void setUserEmail(String email) {
        this.userEmail = email;
    }
}