package TagHandler;

import cse308.FriendRequest;
import cse308.FriendRequestId;
import cse308.PlannerMethods;
import java.util.ArrayList;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Crazzykid
 */
public class FriendRequestTagHandler extends SimpleTagSupport {
    private String userEmail;
    
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();
        
        try {
            
            JspFragment f = getJspBody();
            
            //Querying the DB for all new Friend request
            SessionFactory sFactory = 
                    new Configuration().configure().buildSessionFactory();
            Session openSession = sFactory.openSession();
            
            String sql_FriendRequest = "from FriendRequest r "
                    + "where r.id.studentTwo = '"+userEmail+"'";
            
            Query queryFriendRequest = 
                    openSession.createQuery(sql_FriendRequest);
            
            ArrayList<FriendRequest> requestList = 
                    (ArrayList<FriendRequest>)queryFriendRequest.list();
            openSession.close();
            
            FriendRequestId friend = new FriendRequestId();
            for(int i=0; i<requestList.size(); i++)
            {
                String eMail;
                FriendRequest requestItem = requestList.get(i);
                friend = requestItem.getId();
                eMail = friend.getStudentOne();
                String name = (String)PlannerMethods.getUserList().get(eMail);
                
                // <span class="glyphicon glyphicon-remove del" onclick="deleteRequest('${email}')"></span>
                 
                 
               // String friendRequest = "<li><a role='menuitem' tabindex='-1' > <span class='glyphicon glyphicon-user'></span> " + 
               //         name +" <span class='glyphicon glyphicon-plus add onclick=('AppproveRequestServlet?from=" + 
               //         eMail+"&to="+userEmail+"' ')'>"
               //         + "</span><span class='glyphicon glyphicon-remove del' onclick=('DeleteFriendRequestServlet?from=" + 
               //         eMail+"&to="+userEmail+"' ')'></span> </a> </li>";
                String queryString =    "?from=" + eMail+ "&to=" + userEmail;
                
                getJspContext().setAttribute("name", name);
                getJspContext().setAttribute("email", eMail);
                getJspContext().setAttribute("userEmail", userEmail);
                getJspContext().setAttribute("queryString", queryString);
            
                String friendRequest = "<li><a role='menuitem' tabindex='-1' "
                        + "href ='http://localhost:8080/cse308hw6/ApproveRequestServlet?from=" + 
                        eMail+"&to="+userEmail+"'> <span class='glyphicon glyphicon-user'></span> " + 
                        name +" <div style=\"float: right;\"> <span class='glyphicon glyphicon-plus add'>"
                        + "</span><span class='glyphicon glyphicon-remove del'></div></span> </a> </li>";
                getJspContext().setAttribute("friendRequest", friendRequest);
                if (f != null) {
                    f.invoke(out);
                }
            }
            if(requestList.size()>0)
            {
                String friendRequest =" <li role='presentation' class='divider'></li><li class = 'all' role='presentation'><a role='menuitem' tabindex='-1' href='#'><span class='glyphicon glyphicon-ok'></span> Approve All</a></li>";
               // getJspContext().setAttribute("friendRequest", friendRequest);
               // if (f != null) {
               //     f.invoke(out);
              //  }
            }
            
        } catch (java.io.IOException ex) {
            throw new JspException("Error in FriendRequestTagHandler tag", ex);
        }
    }
    
    public void setUserEmail(String email) {
        this.userEmail = email;
    }
}