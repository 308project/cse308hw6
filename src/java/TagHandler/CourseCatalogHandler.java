package TagHandler;

import cse308.Section;
import cse308.Student;
import java.util.ArrayList;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.JspFragment;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author ShawnCruz
 */
public class CourseCatalogHandler extends SimpleTagSupport {

    private String userEmail;
    
    @Override
    public void doTag() throws JspException {
        JspWriter out = getJspContext().getOut();
        
        try {
            
            JspFragment f = getJspBody();
            
            String fEmail = userEmail;
            SessionFactory sFactory =
                    new Configuration().configure().buildSessionFactory();
            Session openSession = sFactory.openSession();
 
            String hqlSections = "from Student S, Section C"
                    + " where S.schoolCode = C.schoolCode"
                    + " and S.emailAddress = '" + userEmail + "'";
            Query sectionsQuery = openSession.createQuery(hqlSections);
            ArrayList<Object[]> sectionsList
                    = (ArrayList<Object[]>) sectionsQuery.list();
            openSession.close();

            Student student;
            Section section;
            String courseName, courseCode, sectionNumber, courseElement, uniqueCourse;
            int multiSelectIndex = 0;
            ArrayList<String> courseCatalog = new ArrayList<>();
            //Displaying the result of the DB in the Request Tag on the admin's page
            for (Object[] sectionItem : sectionsList) {
                student = (Student) sectionItem[0];
                section = (Section) sectionItem[1];

                courseName = section.getCourseName();
                courseCode = section.getCourseCode();
                //ONLY ADDS COURSE NAMES ONCE
                if (!courseCatalog.contains(courseName)) {
                    courseCatalog.add(section.getCourseName());
                    courseElement = "<li><a><strong>" + courseName + "</strong> "
                            + "(" + courseCode + ")</a></li>";
                    getJspContext().setAttribute("courseElement", courseElement);
                    if (f != null) {
                        f.invoke(out);
                    }
                }
            }

        } catch (java.io.IOException ex) {
            throw new JspException("Error in CourseListHandler tag", ex);
        }
    }
    
    public void setUserEmail(String email) {
        this.userEmail = email;
    }
}
