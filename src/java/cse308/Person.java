package cse308;
// Generated Apr 27, 2015 12:10:33 AM by Hibernate Tools 4.3.1



/**
 * Person generated by hbm2java
 */
public class Person  implements java.io.Serializable {


     private PersonId id;

    public Person() {
    }

    public Person(PersonId id) {
       this.id = id;
    }
   
    public PersonId getId() {
        return this.id;
    }
    
    public void setId(PersonId id) {
        this.id = id;
    }




}


