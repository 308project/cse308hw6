package cse308;

import java.util.HashMap;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import java.util.ArrayList;

public class PlannerMethods {
    
    public static HashMap getUserList(){
        
        Query query;
        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Transaction openTransaction = openSession.beginTransaction();
        String sql_query = "from User" ;
        query = openSession.createQuery(sql_query);
        List list = query.list();
        openSession.close();
        User u = new User();
        
        HashMap userMap = new HashMap();
        {
            for(int i=0; i<list.size(); i++)
            {
                User map = (User)list.get(i);
                String name = map.getFirstName() + " "+ map.getLastName();
                userMap.put(map.getEmailAddress(),name)  ;
            }
        }
        return userMap;
    }
    
    public static List getSearchList(String search){
        Query query;
        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Transaction openTransaction = openSession.beginTransaction();
        String sql_querySchool = "from School ";
        String sql_queryStudent = "from Student ";
        String sql_query = "from User ";
        query = openSession.createQuery(sql_query);
        Query queryStudents = openSession.createQuery(sql_queryStudent);
        Query querySchool = openSession.createQuery(sql_querySchool);
        List searchSchool = querySchool.list();
        List searchStudent = queryStudents.list();
        openSession.close();
        List<String> studentListSearch = new ArrayList();
        List<String> schoolListSearch = new ArrayList();
        
        for (int i = 0; i < searchSchool.size(); i++) {
            School s =(School) searchSchool.get(i);
            String zipCode = s.getSchoolCode().substring(0,5);
            schoolListSearch.add(s.getSchoolName() +"["+zipCode+"]" );
        }
        for (int i = 0; i < searchStudent.size(); i++) {
            Student s =(Student) searchStudent.get(i);
            studentListSearch.add(PlannerMethods.getUserList().get(s.getEmailAddress()) + " ["+ s.getEmailAddress()+"]");
        }
        if(search.equals("school"))
            return schoolListSearch;
        else
            if(search.equals("student"))
                return studentListSearch;
        return null;
    }
    
}
