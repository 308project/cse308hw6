package cse308;
// Generated May 4, 2015 3:37:07 AM by Hibernate Tools 4.3.1



/**
 * School generated by hbm2java
 */
public class School  implements java.io.Serializable {


     private Integer schoolId;
     private Integer numStudents;
     private String studentList;
     private String academicYearList;
     private String scheduleSchema;
     private String schoolName;
     private String schoolCode;
     private String totalCourseOffering;

    public School() {
    }

	
    public School(String schoolName, String schoolCode) {
        this.schoolName = schoolName;
        this.schoolCode = schoolCode;
    }
    public School(Integer numStudents, String studentList, String academicYearList, String scheduleSchema, String schoolName, String schoolCode, String totalCourseOffering) {
       this.numStudents = numStudents;
       this.studentList = studentList;
       this.academicYearList = academicYearList;
       this.scheduleSchema = scheduleSchema;
       this.schoolName = schoolName;
       this.schoolCode = schoolCode;
       this.totalCourseOffering = totalCourseOffering;
    }
   
    public Integer getSchoolId() {
        return this.schoolId;
    }
    
    public void setSchoolId(Integer schoolId) {
        this.schoolId = schoolId;
    }
    public Integer getNumStudents() {
        return this.numStudents;
    }
    
    public void setNumStudents(Integer numStudents) {
        this.numStudents = numStudents;
    }
    public String getStudentList() {
        return this.studentList;
    }
    
    public void setStudentList(String studentList) {
        this.studentList = studentList;
    }
    public String getAcademicYearList() {
        return this.academicYearList;
    }
    
    public void setAcademicYearList(String academicYearList) {
        this.academicYearList = academicYearList;
    }
    public String getScheduleSchema() {
        return this.scheduleSchema;
    }
    
    public void setScheduleSchema(String scheduleSchema) {
        this.scheduleSchema = scheduleSchema;
    }
    public String getSchoolName() {
        return this.schoolName;
    }
    
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    public String getSchoolCode() {
        return this.schoolCode;
    }
    
    public void setSchoolCode(String schoolCode) {
        this.schoolCode = schoolCode;
    }
    public String getTotalCourseOffering() {
        return this.totalCourseOffering;
    }
    
    public void setTotalCourseOffering(String totalCourseOffering) {
        this.totalCourseOffering = totalCourseOffering;
    }




}


