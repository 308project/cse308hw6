package cse308;
// Generated Apr 27, 2015 2:35:19 AM by Hibernate Tools 4.3.1



/**
 * User generated by hbm2java
 */
public class User  implements java.io.Serializable {


     private String emailAddress;
     private String firstName;
     private String lastName;
     private String password;
     private String role;
     private String accountApproved;

    public User() {
    }

    public User(String emailAddress, String firstName, String lastName, String password) {
       this.emailAddress = emailAddress;
       this.firstName = firstName;
       this.lastName = lastName;
       this.password = password;
    }
   
    public String getEmailAddress() {
        return this.emailAddress;
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    public String getRole() {
        return this.role;
    }
    
    public void setRole(String role) {
        this.role = role;
    }
    public String getAccountApproved() {
        return this.accountApproved;
    }
    
    public void setAccountApproved(String accountApproved) {
        this.accountApproved = accountApproved;
    }




}


