package cse308;
import java.util.logging.Logger;

public class InfoLog {
    private static InfoLog singleton = null;
    private static final Logger logger = 
                Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    
    private InfoLog () {
    }
    
    public static InfoLog getInfoLog()
    {
        if (singleton == null)
            singleton = new InfoLog();
        return singleton;
    }
    
    public Logger getLogger()
    {
        return logger;
    }
}
