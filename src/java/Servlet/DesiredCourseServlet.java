/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import cse308.ScheduleBlock;
import cse308.ScheduleBlockId;
import cse308.Section;
import cse308.Student;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Crazzykid
 */
@WebServlet(name = "DesiredCourseServlet", urlPatterns = {"/DesiredCourseServlet"})
public class DesiredCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

            //MAP&ALIST STORE STUDENT'S DESIRED COURSE SELECTIONS
            Map<Section, ArrayList<Section>> selectedSectionMap = new HashMap<>();
            ArrayList<Section> selectedSectionList = new ArrayList<>();

            //OPEN DB CONNECTION
            String userName = (String) request.getSession().getAttribute("userName");
            String schoolCode = "";
            SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session openSession = sessionFactory.openSession();
            Transaction transaction = null;

            //QUERY DB FOR LIST
            String query = "from Student S where S.emailAddress = '" + userName + "' ";
            Query q = openSession.createQuery(query);
            List list = q.list();

            //RETRIEVE SCHOOLCODE OF STUDENT
            for (int i = 0; i < list.size(); i++) {
                Student s = (Student) list.get(i);
                schoolCode = s.getSchoolCode();
            }
            //RETRIEVE SECTION LIST
            int year = Calendar.getInstance().get(Calendar.YEAR);
            String hql = "from Section S where S.schoolCode ='" + schoolCode + "' AND S.year='" + year + "' ";
            q = openSession.createQuery(hql);
            List section = q.list();
            //PARSE SECTION FOR COURSECODE
            String[] result = request.getParameterValues("my-select[]");
            for (int i = 0; i < section.size(); i++) {
                Section s = (Section) section.get(i);
                for (int k = 0; k < result.length; k++) {
                    String[] res = result[k].split("-");
                    String courseCode = res[0];
                    int sectionNum = Integer.parseInt(res[1]);
                    //FILTER ALL COURSES BY STUDENT'S SELECTIONS
                    if (courseCode.equals(s.getCourseCode()) && sectionNum == s.getSectionNumber()) {
                        ArrayList<Section> sectionArray = new ArrayList<>();
                        selectedSectionList.add(s);
                        selectedSectionMap.put(s, sectionArray);
                    }
                }
            }
            //FILL MAP WITH CONFLICTING VALUES
            for (int i = 0; i < selectedSectionList.size() - 1; i++) {
                for (int j = i + 1; j < selectedSectionList.size(); j++) {
                    //PARSE VALUES INTO DISCERNIBLE PIECES 
                    Section subject = selectedSectionList.get(i);
                    Section object = selectedSectionList.get(j);
                    String subjectScheduleBlock = subject.getScheduleBlock();
                    String objectScheduleBlock = object.getScheduleBlock();
                    String[] subjectSplit = subjectScheduleBlock.split(":");
                    String[] objectSplit = objectScheduleBlock.split(":");
                    String subjectDays = subjectSplit[1];
                    String objectDays = objectSplit[1];
                    String[] subjectDayList = subjectDays.split(";");
                    String[] objectDayList = objectDays.split(";");
                    ArrayList<String> objectDayArrayList = new ArrayList();
                    for (String day : objectDayList) {
                        objectDayArrayList.add(day);
                    }
                    for (int k = 0; k < subjectDayList.length; k++) {
                        if (objectDayArrayList.contains(subjectDayList[k])) {
                            selectedSectionMap.get(subject).add(object);
                            selectedSectionMap.get(object).add(subject);
                            break;
                        }
                    }
                }
            }//MAP FILL ENDS

            int longestValue = 1;
            Section key = null;
            //MASTER LOOP CONDITION
            while (longestValue > 0) {
                longestValue = 0;
                //STEP 1: FIND KEY WITH LONGEST VALUE LIST
                for (int i = 0; i < selectedSectionMap.size(); i++) {
                    int trialValue = selectedSectionMap.get(selectedSectionList.get(i)).size();
                    if (trialValue > longestValue) {
                        longestValue = trialValue;
                        key = selectedSectionList.get(i);
                    }
                }
                //IF TRUE, EXIT LOOP (STEP 4B)
                if (longestValue == 0) {
                    continue;
                }
                //STEP 2: DELETE KEY FROM OTHER KEY'S VALUE LIST
                for (int i = 0; i < selectedSectionMap.size(); i++) {
                    if (!key.equals(selectedSectionList.get(i))) {
                        ArrayList<Section> selectedValue = selectedSectionMap.get(selectedSectionList.get(i));
                        for (int j = 0; j < selectedValue.size(); j++) {
                            if (key.equals(selectedValue.get(j))) {
                                selectedValue.remove(j);
                            }
                        }
                    }
                }
                //STEP 3: DELETE K,V PAIR FROM MAP / DEBUGGING AID
                System.out.println("Section to remove: " + key.getCourseCode() + "-" + key.getSectionNumber());
                ArrayList<Section> sList = selectedSectionMap.remove(key);
                Section s2 = selectedSectionList.remove(selectedSectionList.indexOf(key));
                System.out.println("");
            }
            
            //FOREACH SECTION CREATE A SCHEDULE BLOCK AND ADD TO THE DB
            for (Section s : selectedSectionList) {
                try {
                    transaction = openSession.beginTransaction();
                    int sectionId = s.getId();
                    String semester = "1";
                    String scheduleBlock = s.getScheduleBlock();
                    String email = userName;
                    String role = "Desired";
                    ScheduleBlockId ids = new ScheduleBlockId(year, semester, scheduleBlock, sectionId, email, role);
                    ScheduleBlock schedule = new ScheduleBlock(ids);
                    openSession.save(schedule);
                    transaction.commit();
                } catch (HibernateException ex) {
                    if (transaction != null) {
                        transaction.rollback();
                    }
                } finally {

                }
                
            }
            openSession.close();
            //RETURN TO STUDENT.JSP WHEN FINISHED
            if (!response.isCommitted()) {
                    response.sendRedirect("student.jsp");
                }
                
           
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
