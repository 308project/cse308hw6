package Servlet;

import cse308.Friends;
import cse308.InfoLog;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "ApproveRequestServlet", urlPatterns = {"/ApproveRequestServlet"})
public class ApproveRequestServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        
        InfoLog log = InfoLog.getInfoLog();
        Logger logger = log.getLogger();
        
        String fromEmail = request.getParameter("from");
        String toEmail = request.getParameter("to");
        
        //Creating Session to store Student
        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Transaction openTransaction = null;
        
        try {
            openTransaction = openSession.beginTransaction();
            String friendCheck = " from Friends F where F.friendEmail = '"+fromEmail+"' AND F.userEmail = '"+toEmail+"'"  ;
            Query checkQuery  = openSession.createQuery(friendCheck);
            List check = checkQuery.list();
            
            if(!check.isEmpty())
                response.sendRedirect("student.jsp");
            else
                if (check.isEmpty()){
                    friendCheck = " from Friends F where F.friendEmail = '"+toEmail+"' AND F.userEmail = '"+fromEmail+"'"  ;
                    checkQuery  = openSession.createQuery(friendCheck);
                    List checkList = checkQuery.list();
                    if(!checkList.isEmpty())
                        response.sendRedirect("student.jsp");
                }
            Friends f = new Friends (fromEmail, toEmail);
            openSession.saveOrUpdate(f);
            logger.log(Level.INFO, "Friend Relation successfully accepted "
                    + "between {0} to {1}", new Object[]{fromEmail, toEmail});

            String deleteHQL = "delete from FriendRequest F where F.id.studentOne = '"+fromEmail+"' AND F.id.studentTwo = '"+toEmail+"'"  ;
            Query query = openSession.createQuery(deleteHQL);
            query.executeUpdate();
            openTransaction.commit();
            logger.log(Level.INFO, "Friend Request successfully deleted from DB"
                    + " between {0} to {1}", new Object[]{fromEmail, toEmail});
        }catch (HibernateException ex) {
            if (openTransaction != null)
                openTransaction.rollback();
            ex.printStackTrace();
        }finally {
            openSession.close();
        }
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Student Home</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Friend Request Successfully Accepted.'); "
                    + "window.location.href = \"student.jsp\"");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");
        }
        response.sendRedirect("student.jsp");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}