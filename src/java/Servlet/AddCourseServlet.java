package Servlet;

import cse308.ScheduleBlock;
import cse308.ScheduleBlockId;
import cse308.Section;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "AddCourseServlet", urlPatterns = {"/AddCourseServlet"})
public class AddCourseServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        
        String email = (String) request.getSession().getAttribute("userName");
      
        String courseName = request.getParameter("courseName");
        String professor = request.getParameter("courseInstructor");
        String courseSection = request.getParameter("courseSection");
        String[] splitRange = request.getParameterValues("semesterRange[]");
        String semesterRange = "semester"+splitRange[0]+",semester"+splitRange[1];
        
        
        String scheduleBlock = request.getParameter("scheduleBlock");
        String semester = request.getParameter("semester").substring(8);
        
       
        String courseCode = request.getParameter("courseCode");
        int sectionNumber = Integer.parseInt(courseSection);
        String code = request.getParameter("academicYr");
        String[] split = code.split(",");
        int year = Integer.parseInt(split[0]);
        String schoolCode = split[1];
        
        boolean sectionAdd = false;
        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session openSession = sessionFactory.openSession();
        Transaction transaction = null;
        String query = "from Section S where S.schoolCode = '" + schoolCode + "' AND  S.courseCode = '"+courseCode+"' ";
        Query q = openSession.createQuery(query);
        List list = q.list();
        
        for(int i=0; i<list.size(); i++){
           Section section = (Section)list.get(i);
         if(section.getYear()==year && section.getProfessorName().equals(professor) && section.getSectionNumber()==Integer.parseInt(courseSection))
                sectionAdd = true;
        }
        if(list.size()==0)
        {
            
            try {
                int sec = Integer.parseInt(courseSection);
                transaction = openSession.beginTransaction();
                Section s = new Section(sec , courseCode, professor, year, semesterRange, schoolCode,courseName,scheduleBlock);
                s.setCourseDescription("test,test test,test test,test test,test test,test test,test");
                openSession.save(s);
                transaction.commit();
            } catch (HibernateException ex) {
                if (transaction != null)
                    transaction.rollback();
            } finally {
                openSession.close();
            }
            if(!response.isCommitted()){
                RequestDispatcher dispatcher = request.getRequestDispatcher("/AddCourseServlet");
                dispatcher.forward(request, response);
            }
        }
        else
        {
            for(int i =0; i<list.size(); i++){
                Section section = (Section)list.get(i);
                if(section.getYear()==year && section.getProfessorName().equals(professor) && section.getSectionNumber()==Integer.parseInt(courseSection)){
                    
                    try {
                        transaction = openSession.beginTransaction();
                        int sectionId = section.getId();
                        String role ="Assigned";
                        ScheduleBlockId ids = new ScheduleBlockId(year, semester, scheduleBlock, sectionId, email, role);
                        ScheduleBlock schedule = new ScheduleBlock(ids);
                        openSession.save(schedule);
                        transaction.commit();
                    } catch (HibernateException ex) {
                        if (transaction != null)
                            transaction.rollback();
                    } finally {
                        openSession.close();
                    }
                    if(!response.isCommitted())
                        response.sendRedirect("student.jsp");
                    break;
                }
                else
                if(section.getYear()!=year && !sectionAdd ){
                    try {
                        transaction = openSession.beginTransaction();
                        int sec = Integer.parseInt(courseSection);
                        Section s = new Section(sec, courseCode, professor, year, semesterRange, schoolCode,courseName,scheduleBlock);
                        s.setCourseDescription("test,test test,test test,test test,test test,test test,test");
                        openSession.save(s);
                        transaction.commit();
                    } catch (HibernateException ex) {
                        if (transaction != null)
                            transaction.rollback();
                    } finally {
                        openSession.close();
                        
                    }
                    if(!response.isCommitted()){
                        RequestDispatcher dispatcher = request.getRequestDispatcher("/AddCourseServlet");
                        dispatcher.forward(request, response);
                        break;
                    
                }
                
            }
               
        }
                  
                    }
    }
    // <editor-fold defaultstate="collapsed" test desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}