/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ShawnCruz
 */
@WebServlet(name = "AddSemestersServlet", urlPatterns = {"/AddSemestersServlet"})
public class AddSemestersServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //PARSE NUMBER OF SEMESTERS DATA TO BE USED LATER
            int numSemesters = Integer.parseInt(request.getParameter("numSem"));
            //PAGE IMPORT STATEMENTS
           
            
            
            
            
            
            out.println("<%@page import=\"cse308.School\"%>\n"
                    + "<%@page import=\"java.util.List\"%>\n"
                    + "<%@page import=\"org.hibernate.Transaction\"%>\n"
                    + "<%@page import=\"org.hibernate.Session\"%>\n"
                    + "<%@page import=\"org.hibernate.SessionFactory\"%>\n"
                    + "<%@page import=\"cse308.Student\"%>\n"
                    + "<%@page import=\"cse308.User\"%>\n"
                    + "<%@page import=\"java.util.ArrayList\"%>\n"
                    + "<%@page import=\"org.hibernate.cfg.Configuration\"%>\n"
                    + "<%@page import=\"org.hibernate.Query\"%>\n"
                    + "<%@page contentType=\"text/html\" pageEncoding=\"UTF-8\"%>");
            out.println("<!DOCTYPE html>");
            out.println("<html>\n"
                    + "    <head>\n"
                    + "        <meta charset=\"UTF-8\">\n"
                    + "        <title>Add Academic Year Page</title>\n"
                    + "        <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>\n"
                    + "        <link rel=\"stylesheet\" href=\"css/reg_style.css\">\n"
                    + "        <script type=\"text/javascript\" src=\"./js/jquery.js\"></script>\n"
                    + "        <script type='text/javascript' src='./js/jquery.tipsy.js'></script>\n"
                    + "        <link rel=\"stylesheet\" href=\"./css/tipsy.css\" type=\"text/css\"/>\n"
                    + "    </head>\n"
                    + "    \n"
                    + "   \n"
                    + "    \n"
                    + "    <body>\n"
                    + "        <div class=\"register-card\">\n"
                    + "            <h1>Add Academic Year</h1><br>\n"
                    + "            <form action=\"AddAcademicYearServlet\">");
            
            out.println("<head>");
            out.println("<title>Servlet AddSemestersServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddSemestersServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
