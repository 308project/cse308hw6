/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlet;

import cse308.School;
import cse308.User;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author ShawnCruz
 */
@WebServlet(name = "AddSchoolServlet", urlPatterns = {"/AddSchoolServlet"})
public class AddSchoolServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
            String schoolName = "";
            String schoolCode = "";
            String zipCode = "";
            schoolName = request.getParameter("school");
            zipCode = request.getParameter("zip");
            schoolCode = zipCode + schoolName;
            
            //Creating Session to store User
            SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
            Session openSession = sFactory.openSession();
            Transaction openTransaction = null;
            
            try {
                openTransaction = openSession.beginTransaction();
                
                School school = new School(schoolName,schoolCode);
                openSession.save(school);
                openTransaction.commit();
                
            } catch (HibernateException ex) {
                if (openTransaction != null) {
                    openTransaction.rollback();
                }
                ex.printStackTrace();
            } finally {
                openSession.close();
            }

            /* TODO output your page here. You may use following sample code. */
             out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Registration</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<script type=\"text/javascript\">");  
            out.println("alert('You have successfully added a school'); window.location.href = \"admin.jsp\"");  
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
