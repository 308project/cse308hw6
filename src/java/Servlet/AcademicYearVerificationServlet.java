/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import cse308.AcademicYear;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Crazzykid
 */
@WebServlet(name = "AcademicYearVerificationServlet", urlPatterns = {"/AcademicYearVerificationServlet"})
public class AcademicYearVerificationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String schoolCode = request.getParameter("q");
        Query query;
        String school = "";
        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Transaction openTransaction = openSession.beginTransaction();
        String sql_query = "from AcademicYear A where A.id.schoolCode ='"+schoolCode+"' ";
        query = openSession.createQuery(sql_query);
        List list = query.list();
        openSession.close();
        AcademicYear a = new AcademicYear();
        String cycle ="";
        int numSemester = 0;
        String lunchFrom = "";
        String lunchTo ="";
        String block = "";
        int numPeriod = 0;
        
        
        for (int i = 0; i < list.size(); i++) {
            a = (AcademicYear)list.get(i);
            cycle = a.getScheduleCycle();
            numSemester = a.getNumSemesters();
            String[] lunch = a.getLunchRange().split(",");
            lunchFrom = lunch[0];
            lunchTo = lunch[1];
            numPeriod = a.getNumPeriods();
            block = a.getScheduleBlock();
        break;
        }
        try (PrintWriter out = response.getWriter()) {
            
            out.print(numSemester +"-"+numPeriod +"-"+ cycle +"-"+ lunchFrom+"-"+lunchTo+"-"+block);
            
            
        }
        
        
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
