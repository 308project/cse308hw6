package Servlet;

import cse308.FriendRequest;
import cse308.FriendRequestId;
import cse308.InfoLog;
import cse308.Student;
import cse308.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "FriendRequestServlet", urlPatterns = {"/FriendRequestServlet"})
public class FriendRequestServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        //Creating Session to store User
        SessionFactory sFactory =
                new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Transaction openTransaction = null;
        
        InfoLog log = InfoLog.getInfoLog();
        Logger logger = log.getLogger();
        
        try {
            String email = request.getParameter("friend");
            HttpSession session = request.getSession(true);
            String senderEmail = (String)session.getAttribute("userName");
            openTransaction = openSession.beginTransaction();
            String sql_query = "from Student S, User U where S.emailAddress =  '"+ email +"' AND U.emailAddress = '"+ email +"'  " ;
            
            //querying database to prevent duplicate friends
            String friendquery = "from Friends F where (F.userEmail = '"+ email +"' AND F.friendEmail = '"+ senderEmail +"' ) OR (F.userEmail = '"+ senderEmail +"' AND F.friendEmail = '"+ email +"' )" ;
            String sql_FriendRequest = "from FriendRequest R where (R.id.studentTwo = '"+email+"' AND R.id.studentTwo = '"+ senderEmail +"') OR (R.id.studentTwo = '"+senderEmail+"' AND R.id.studentTwo = '"+ email +"') ";
            Query query = openSession.createQuery(friendquery);
            Query sql = openSession.createQuery(sql_FriendRequest);
            if(query.list().isEmpty() && sql.list().isEmpty())
            {
                query = openSession.createQuery(sql_query);
                ArrayList<Object[]> studentList =
                        (ArrayList<Object[]>)query.list();
                User studentUser = new User();
                Student student = new Student();
                for (Object[] tableItem : studentList) {
                    student = (Student)tableItem[0];
                    studentUser = (User)tableItem[1];
                }
                
                if(student != null && (!senderEmail.equals(student.getEmailAddress())))
                {
                    String receiverEmail = student.getEmailAddress();
                    FriendRequestId friend =
                            new FriendRequestId (senderEmail, receiverEmail);
                    FriendRequest f = new FriendRequest(friend);
                    openSession.save(f);
                    openTransaction.commit();
                    logger.log(Level.INFO, "Friend Request successfully sent "
                            + "from {0} to {1}.",
                            new Object[]{senderEmail, receiverEmail});
                }
            }
        } catch (HibernateException ex) {
            if (openTransaction != null)
                openTransaction.rollback();
            ex.printStackTrace();
        } finally {
            openSession.close();
            
        }

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Student Home</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('Friend Request Successfully Sent.'); "
                    + "window.location.href = \"student.jsp\"");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");
        }
        response.sendRedirect("student.jsp");
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}