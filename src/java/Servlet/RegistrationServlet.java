package Servlet;

import cse308.InfoLog;
import cse308.Student;
import cse308.User;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "RegistrationServlet", urlPatterns = {"/RegistrationServlet"})
public class RegistrationServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        
        String email = request.getParameter("email");
        String pWord = request.getParameter("pass");
        String fName = request.getParameter("fname");
        String lName = request.getParameter("lname");
        String sex = request.getParameter("sex");
        String school = request.getParameter("schoolselect");
        
        InfoLog log = InfoLog.getInfoLog();
        Logger logger = log.getLogger();
        
        // Creating Session to store User
        SessionFactory sFactory =
                new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Transaction openTransaction = null;
        
        try {
            openTransaction = openSession.beginTransaction();
            User u = new User(email, fName, lName, pWord);
            u.setRole("Student");
            u.setAccountApproved("0");
            openSession.save(u);
            openTransaction.commit();
            logger.log(Level.INFO,
                    "User successfully created and inserted into DB");
        } catch (HibernateException ex) {
            if (openTransaction != null)
                openTransaction.rollback();
            ex.printStackTrace();
        } finally {
            openSession.close();
        }
        
        //Creating Session to store Student
        sFactory = new Configuration().configure().buildSessionFactory();
        openSession = sFactory.openSession();
        openTransaction = null;
        
        try {
            openTransaction = openSession.beginTransaction();
            Student s = new Student(email, sex, school);
            openSession.save(s);
            openTransaction.commit();
            logger.log(Level.INFO,
                    "Student successfully created and inserted into DB.");
        } catch (HibernateException ex) {
            if (openTransaction != null)
                openTransaction.rollback();
            ex.printStackTrace();
        } finally {
            openSession.close();
        }
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Registration</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<script type=\"text/javascript\">");
            out.println("alert('You have successfully registered'); "
                    + "window.location.href = \"index.jsp\"");
            out.println("</script>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}