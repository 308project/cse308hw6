/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlet;

import cse308.AcademicYear;
import cse308.Friends;
import cse308.ScheduleBlock;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author ShawnCruz
 */
@WebServlet(name = "DisplayCourseWithFriendsServlet", urlPatterns = {"/DisplayCourseWithFriendsServlet"})
public class DisplayCourseWithFriendsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
           
                     int  year = Calendar.getInstance().get(Calendar.YEAR);

                String email = "lamar.myles@stonybrook.edu";   //(String) request.getSession().getAttribute("userName");
                SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
                Session openSession = sessionFactory.openSession();
                Transaction transaction = null;
                String query_sql_ScheduleBlock = "from ScheduleBlock S where S.id.emailAddress = '" + email + "' AND  S.id.year = '" + year + "' ";
                String querySchool_Academic = "from AcademicYear A, Student S where S.emailAddress = '" + email + "' AND  A.id.year = '" + year + "' AND S.schoolCode = A.id.schoolCode";
                String querySection_sql = "from Section";
                Query sectionQuery_sql = openSession.createQuery(querySection_sql);
                Query queryhql = openSession.createQuery(query_sql_ScheduleBlock);
                Query query_school_sql = openSession.createQuery(querySchool_Academic);
                
                List schedule_result = queryhql.list();
                List section_result = sectionQuery_sql.list();
        
                ArrayList<Object[]> academicList_result
                    = (ArrayList<Object[]>) query_school_sql.list();

            AcademicYear academic_reslut = new AcademicYear();
            int numCycle = 0;
            //Displaying the result of the DB in the Request Tag on the admin's page
            for (Object[]  item : academicList_result) {
                academic_reslut = (AcademicYear)item[0];
                numCycle= Integer.parseInt(academic_reslut.getScheduleCycle());
                
            }
         
            
            String hqlFriends = "from Friends F where (F.userEmail = "
                    + "'"+email+"') OR ( F.friendEmail='"+email+"')";
            
            Query friendQuery = openSession.createQuery(hqlFriends);
            ArrayList<Friends> friendsList = 
                    (ArrayList<Friends>)friendQuery.list();
            ArrayList<String> friendsCourseArray = new ArrayList<String>();
            //Displaying the result of the DB in the Request Tag on the admin's page
            for (Friends friend : friendsList) {
                String eMail = friend.getFriendEmail();
                String myEmail = friend.getUserEmail();
                if(!eMail.equals(email))
                    friendsCourseArray.add(eMail);
                 else
                    if(!myEmail.equals(email))
                         friendsCourseArray.add(myEmail);
                
                String hqlSchedule = "from ScheduleBlock";
                Query scheduleQuery = openSession.createQuery(hqlSchedule);
                List friendSchedule = scheduleQuery.list();
                for(int i=0; i<friendSchedule.size(); i++){
                    ScheduleBlock block = (ScheduleBlock)friendSchedule.get(i);
                    for(int j=0; j<friendsCourseArray.size(); j++){
                        String scheduleEmail = friendsCourseArray.get(j);
                        if(scheduleEmail.equals(block.getId().getEmailAddress()))
                        {
                            for(int m =0; m<schedule_result.size();m++){
                                 ScheduleBlock sch = (ScheduleBlock) schedule_result.get(m);
                                 if(sch.getId().getSectionId()== block.getId().getSectionId()){
                               
                                   String time = sch.getId().getTimeSlot();
                             String[] splitScheduleBlock = time.split(":");
                            String period = splitScheduleBlock[0];
                            String numDays = splitScheduleBlock[1];
                            String[] daysArray = numDays.split(";");
                            System.out.print("Days array" + daysArray.length);
                            System.out.print("period " + period);
                            System.out.print("numDays" + daysArray);

                           
                            for (int x = 0; x < daysArray.length; x++) {
                                String[] cellArray = new String[daysArray.length];
                                 String[] cellArray2 = new String[daysArray.length];
                                  String[] cellArray3 = new String[daysArray.length];
                                cellArray[j] = "a" + period + daysArray[j];
                                cellArray2[j] = "a"+ period + (Integer.parseInt(daysArray[j]) +numCycle);
                                cellArray3[j] = "a"+ period + (Integer.parseInt(daysArray[j]) +numCycle+numCycle);
                                String str = "$(\"#";
                                out.print(str + cellArray[j] + "\").append(\" " + scheduleEmail + "<br>\").css(\"background-color\", \"green\");");
                                out.print(str + cellArray2[j] + "\").append(\" " + scheduleEmail + "<br>\").css(\"background-color\", \"green\");");
                                out.print(str + cellArray3[j] + "\").append(\" " + scheduleEmail + "<br>\").css(\"background-color\", \"green\");");

                                System.out.print("cellArray" + cellArray[j]);
                                System.out.print("cellArray2" + cellArray2[j]);
                                System.out.print("cellArray3" + cellArray3[j]);
                            }
                       
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                   
                                 }
                            }
                        }
                    }
                
                    
                }
            }
        
        
         
        
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DisplayCourseWithFriendsServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DisplayCourseWithFriendsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
