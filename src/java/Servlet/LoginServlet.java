package Servlet;

import cse308.InfoLog;
import cse308.PlannerMethods;
import cse308.User;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {
    
    private static HttpSession sc;
    
    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        
        String pWord = request.getParameter("pass");
        String email = request.getParameter("email");
        
        HttpSession session = request.getSession(true);
        SessionFactory sFactory =
                new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        
        InfoLog log = InfoLog.getInfoLog();
        Logger logger = log.getLogger();
        
        String sql_query = "from User " ;
        Query query = openSession.createQuery(sql_query);
        List list = query.list();
        User u;
        
        for (Object list1 : list) {
            u = (User) list1;
            if(u.getEmailAddress().equals(email) &&
                    u.getPassword().equals(pWord) &&
                    u.getAccountApproved().equals("1"))
            {
                session.setAttribute("userName", email);
                session.setAttribute("role", u.getRole());
                sc = session;
                
                if(u.getRole().equals("Student"))
                {
                    response.sendRedirect("student.jsp");
                    logger.log(Level.INFO,
                            "Successfully Logged in as Student: {0}", email);
                }
                else if(u.getRole().equals("Admin"))
                {
                    response.sendRedirect("admin.jsp");
                    logger.log(Level.INFO,
                            "Successfully Logged in as Admin: {0}", email);
                }
                return;
            }
        }
        openSession.close();
        response.sendRedirect("index.jsp");
        logger.info("Could not Log in with Specified Information.");
    }
    
    public static HttpSession getCurrentSession(){
        return sc;
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}