/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import cse308.ScheduleBlock;
import cse308.Section;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Crazzykid
 */
@WebServlet(name = "DisplayScheduleServlet", urlPatterns = {"/DisplayScheduleServlet"})
public class DisplayScheduleServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
                      try (PrintWriter out = response.getWriter()) {
                                    int year = 2015;
                                    
                SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
            Session openSession = sessionFactory.openSession();
                Transaction transaction = null;
            String email = "lamar.myles@stonybrook.edu";
                String query_sql = "from ScheduleBlock S where S.id.emailAddress = '" + email + "' AND  S.id.year = '"+year+"' ";
                String querySchool = "from AcademicYear A, Student S where S.emailAddress = '" + email + "' AND  A.id.year = '"+year+"' AND S.schoolCode = A.id.schoolCode";
                String querySection = "from Section";
                Query sectionQuery = openSession.createQuery(querySection);
                Query querysql = openSession.createQuery(query_sql);
                Query query_school = openSession.createQuery(querySchool);
                List l = query_school.list();
                List schedule = querysql.list();
                List section = sectionQuery.list();
                int scheduleYear;
                String semester ="";
                String timeSlot = "";
                int sectionId;
                 String courseName = "";
                  String courseCode = "";
                  int sectionNumber;
                  String scheduleBlock = "";
                for(int i=0; i<schedule.size(); i++){          
                    ScheduleBlock s = (ScheduleBlock)schedule.get(i);
                        scheduleYear = s.getId().getYear();
                        semester = s.getId().getSemester();
                         timeSlot = s.getId().getTimeSlot();
                         sectionId = s.getId().getSectionId();
                    for(int k=0; k<section.size(); k++){
                        Section sec = (Section)section.get(k);
                        if(sectionId == sec.getId()){
                             courseName = sec.getCourseName();
                             courseCode = sec.getCourseCode();
                             sectionNumber = sec.getSectionNumber();
                             scheduleBlock = sec.getScheduleBlock();
                             String[] splitScheduleBlock = scheduleBlock.split(":");
                             String period = splitScheduleBlock[0];
                             String numDays = splitScheduleBlock[1];
                             String[] daysArray = numDays.split(";");
                             System.out.print("Days array"+daysArray.length);
                              System.out.print("period "+period);
                               System.out.print("numDays"+daysArray); 
                
                              
                          out.print("<script>");
                            for(int j=0;j<daysArray.length; j++)
                             {
                                 String[] cellArray = new String[daysArray.length];
                                 cellArray[j] = "a" + period + daysArray[j];
                            
                            
                            String str = "$(\"#";
                              out.print( str+cellArray[j]+"\").append(\" "+courseName +"<br>\").css(\"background-color\", \"blue\");");
                            
                             
                                System.out.print("cellArray"+cellArray[j]); 
                             }
                             out.print("</script>");
             }
                             
                        }
                    }
                    
                    
                    
                
        
        
        
       
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DisplayScheduleServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DisplayScheduleServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
