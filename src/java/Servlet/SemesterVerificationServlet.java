/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package Servlet;

import cse308.AcademicYear;
import cse308.Section;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Crazzykid
 */
@WebServlet(name = "SemesterVerificationServlet", urlPatterns = {"/SemesterVerificationServlet"})
public class SemesterVerificationServlet extends HttpServlet {
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String code = request.getParameter("code");
        String courseCode = request.getParameter("courseCode");
        String[] split = code.split(",");
        int year = Integer.parseInt(split[0]);
        String schoolCode = split[1];
        
        String query = "from Section S where S.schoolCode = '" + schoolCode + "' AND  S.courseCode = '"+courseCode+"' ";
        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Query q = openSession.createQuery(query);
        List list = q.list();
        openSession.close();
        
        
        String courseName = "";
        String instructor = "";
        String section = "";
        String semester = "";
        String scheduleBlock = "";
        
        
        
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            for(int i = 0; i < list.size(); i++ ){
                Section s = (Section) list.get(i);
                if(s.getYear()== year){
                    courseName = s.getCourseName();
                    if(i<list.size()-1){
                        instructor += s.getProfessorName()+",";
                        semester += s.getSemesterRange()+",";
                        scheduleBlock += s.getScheduleBlock()+"|";
                        section += s.getSectionNumber()+",";
                    }
                    else {
                        instructor += s.getProfessorName();
                        semester += s.getSemesterRange();
                        scheduleBlock += s.getScheduleBlock();
                        section += s.getSectionNumber()+"";
                    }
                }
                else
                    courseName = s.getCourseName();
            }
             out.println(courseName +"-"+ section + "-"+ instructor+ "-"+semester+"-"+scheduleBlock);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
    
}
