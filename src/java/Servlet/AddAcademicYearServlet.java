package Servlet;

import cse308.AcademicYear;
import cse308.AcademicYearId;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

@WebServlet(name = "AddAcademicYearServlet", urlPatterns = {"/AddAcademicYearServlet"})
public class AddAcademicYearServlet extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, 
            HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        String accYear = request.getParameter("year");
        String numSem = request.getParameter("numSem");
        String schoolCode = request.getParameter("schoolSelect");
        int numPeriods = Integer.parseInt(request.getParameter("numPer"));
        String schedCycle = request.getParameter("schedCyle");
        String lunchRangeTo = request.getParameter("lunchTo");
        String lunchRangeFrom = request.getParameter("lunchFrom");
        String scheduleBlock = request.getParameter("scheduleBlocks");
        
        String lunchRange = lunchRangeFrom + "," + lunchRangeTo;
        int numSemesters = Integer.parseInt(numSem);
        int year = Integer.parseInt(accYear);
        AcademicYearId id = new AcademicYearId(schoolCode, year);
        
        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
        Session openSession = sFactory.openSession();
        Transaction openTransaction = null;
   
            try {
                openTransaction = openSession.beginTransaction();
                AcademicYear a = new AcademicYear(id, numSemesters, scheduleBlock, lunchRange ,schedCycle, numPeriods);
                openSession.save(a);
                openTransaction.commit();
            }catch (HibernateException ex) {
                if (openTransaction != null)
                    openTransaction.rollback();
                ex.printStackTrace();
            }finally {
                openSession.close();
            }
        
            response.sendRedirect("admin.jsp");
    }  
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>   
}