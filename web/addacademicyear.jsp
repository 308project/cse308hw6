<%-- 
    Document   : addacademicyear
    Created on : Apr 14, 2015, 1:58:12 AM
    Author     : ShawnCruz
--%>

<%@page import="cse308.School"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="cse308.Student"%>
<%@page import="cse308.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.Query"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Academic Year Page</title>
        <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>
        <link rel="stylesheet" href="css/reg_style.css">
        <script type="text/javascript" src="./js/jquery.js"></script>
        <script type='text/javascript' src='./js/jquery.tipsy.js'></script>
        <link rel="stylesheet" href="./css/tipsy.css" type="text/css"/>
    </head>
    
   
    
    <body>
        <div class="register-card">
            <h1>Add Academic Year</h1><br>
            <form action="AddAcademicYearServlet">
                <!--FIX INPUT BOX SIZE-->
                <select onchange = "populateForm(this.value)" name="schoolSelect" id="schoolSelect" rel = "tipsy" title = "Wait! Did you forget to select a school?">
                    <option value="None">Select School</option>
                    <% 
                            Query query;
                            String school = "";
                            SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
                            Session openSession = sFactory.openSession();
                            Transaction openTransaction = openSession.beginTransaction();
                            String sql_query = "from School ";
                            query = openSession.createQuery(sql_query);
                            List list = query.list();
                            openSession.close();
                            School s = new School();
                            for (int i = 0; i < list.size(); i++) {
                                s = (School) list.get(i);
                                out.print("<option value=\"" + (s.getSchoolCode()) + "\">" + s.getSchoolName() + "</option>");
                            }
                            
                            
                      %>
                     
                      
                </select><br>
                <h3 class = "center">Year</h3>
                <input type="text" name="year" id="year" rel = "tipsy" title = "Wait! Did you forget to enter an academic year?">
             
                <select name="numSem" id="numSem" rel = "tipsy" title = "Wait! Did you forget to select a semesters?">
                    <option value="None">Number of Semesters</option>
                    <option value="1" >1 semester</option>
                    <option value="2" >2 semesters</option>
                    <option value="3" >3 semesters</option>
                    <option value="4" >4 semesters</option>
                </select><br>
                
                <select name="numPer" id="numPer" rel = "tipsy" title = "Wait! Did you forget to select a number of periods?" onChange = "populateFrom()">
                     <option value="None">Number of Periods</option>
                     <option value="6" >6-period day</option>
                     <option value="7" >7-period day</option>
                     <option value="8" >8-period day</option>
                     <option value="9" >9-period day</option>
                     <option value="10" >10-period day</option>
                     <option value="11" >11-period day</option>
                     <option value="12" >12-period day</option>
                </select><br>
                <select name="schedCyle" id="schedCyle" rel = "tipsy" title = "Wait! Did you forget to select a schedule cycle?">
                    <option value="None">Schedule Cycle</option>
                    <option value="1" >1-day schedule</option>
                    <option value="2" >2-day schedule</option>
                    <option value="3" >3-day schedule</option>
                    <option value="4" >4-day schedule</option>
                    <option value="5" >5-day schedule</option>
                    <option value="6" >6-day schedule</option>
                    <option value="7" >7-day schedule</option>
                </select><br>
                <hr>
                <h3 class = "center">Lunch Range</h3>
                <select name="lunchFrom" id="lunchFrom" rel = "tipsy" title = "Wait! Did you forget to select a lunch range?" onchange="populateTo()">
                    <option value="None">From</option>
                </select>
                <select name="lunchTo" id="lunchTo" rel = "tipsy" title = "Wait! Did you forget to select a lunch range?">
                    <option value="None">To </option>
                </select><br>
                <!--                <select id="scheduleBlocks" multiple="multiple" size="10" onfocus="buildSchedule()">
                                    <option value="None">None</option>
                                </select>-->
                <hr>
                <h3 class = "center">Schedule Blocks</h3>
                <input type="text" name="scheduleBlocks" id="scheduleBlocks" rel = "tipsy" title = "Wait! Did you forget to enter schedule blocks?">
                <br><br>
                <input type="submit" name="register" class="register register-submit" value="Submit" onmouseover="verifySchool()">
            </form>
        </div>
        <!--        <script src='http://codepen.io/assets/libs/fullpage/jquery_and_jqueryui.js'></script>-->
        <script type='text/javascript'>
            function populateForm(str) {

             console.log(str);
            if(str!="None"){
            var xmlhttp;
            //showScheduleBlock(str);
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {
                
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    var response = xmlhttp.responseText;
                    console.log(response);
                    var schoolInfo = response.split("-");
                    
                var numSem = schoolInfo[0];
                var numPer = schoolInfo[1];
                var schedCycle = schoolInfo[2];
                var lunchFrom = schoolInfo[3];
                var lunchTo = schoolInfo[4];
                var block = schoolInfo[5];
                
                if(numSem != 0){
                $("#numSem").find('option:contains('+numSem+')').attr("selected",true);
                $("#numPer").find('option:contains('+numPer+')').attr("selected",true);
                populateFrom();
                populateTo();
                $("#schedCyle").find('option:contains('+schedCycle+')').attr("selected",true);
                $("#lunchFrom").find('option:contains('+lunchFrom+')').attr("selected",true);
                $("#lunchTo").find('option:contains('+lunchTo+')').attr("selected",true);
                $("#scheduleBlocks").val(block);
            }
            else
                clearData();
                }
                
            }
            xmlhttp.open("GET", "AcademicYearVerificationServlet?q=" + str, true);
            xmlhttp.send();
        }
        else{
            clearData();
        }
        }   
        
        function clearData(){
            $('#numSem option[value="None"]').attr("selected",true);
            $('#numPer option[value="None"]').attr("selected",true);
            $('#schedCyle option[value="None"]').attr("selected",true);
            $('#lunchFrom').append("<option value = 'None'>From</option>");
            $('#lunchTo').append("<option value = 'None'>To</option>");
            $('#lunchFrom option[value="None"]').attr("selected",true);
            $('#lunchTo option[value="None"]').attr("selected",true);
            $('#scheduleBlocks').val("");
        }
      
            function buildSchedule() {
                var buildSchedule = document.getElementById("buildSchedule"); 
                resetOptions(buildSchedule);
                
            }
            
            function resetOptions(elementName) {
               while(elementName.options.length > 0) {
                   elementName.remove(0);
               }
            }
           
            function populateFrom() {
               var lunchFrom = document.getElementById("lunchFrom"); 
               var numPer = document.getElementById("numPer").value;
               resetOptions(lunchFrom);
               for(var i = 1; i < numPer; i++)
               lunchFrom.options[lunchFrom.options.length] = new Option('Period ' + i, i);
            }

           
           function populateTo() {
               var numPer = document.getElementById("numPer").value;
               var lunchFrom = document.getElementById("lunchFrom").value;
               var lunchTo = document.getElementById("lunchTo");
               //alert("lunchFrom: " + lunchFrom + "numPer: " + numPer + "lunchTo: " + lunchTo.value);;
               resetOptions(lunchTo);
               var start = lunchFrom;
               ++start;
               for(var i = start; i <= numPer; i++) 
               {
               lunchTo.options[lunchTo.options.length] = new Option('Period ' + i, i);
             
           }
               //alert(x);
               
            }
            
            $(function() {
                $('select[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
                $('input[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
                });
                      
            var schoolSelect = document.getElementById("schoolSelect");
            var numberSemesters = document.getElementById("numSem");
            var numberDays = document.getElementById("numDays");
            var numberPer = document.getElementById("numPer");
            var rangeLunch = document.getElementById("rangeLunc");
            var legalBlocks = document.getElementById("legalBlocks");
            var academicYear = document.getElementById("acdYr");
            
            function verifySchool(){
                if(schoolSelect.value=="None")
                    $("#schoolSelect").tipsy("show");
                else
                     $("#schoolSelect").tipsy("hide");
            }
            function verifyNumSemesters() {
                if (numberSemesters.value == "") {
                    $("#numSem").tipsy("show");
                }
                else
                    $("#numSem").tipsy("hide");
            }
            function verifyNumDays() {
                if (numberDays.value == "")
                    $("#numDays").tipsy("show");
                else
                    $("#numDays").tipsy("hide");
            }
            function verifyNumPer() {
               if (numberPer.value == "")
                    $("#numPer").tipsy("show");
                else
                    $("#numPer").tipsy("hide");
            }
            function verifyRangeLunc() {
                if (rangeLunch.value == "")
                    $("#rangeLunc").tipsy("show");
                else {
                    $("#rangeLunc").tipsy("hide");
                }
            }
            function verifyLegalBlocks() {
                if (legalBlocks.value == "")
                    $("#legalBlocks").tipsy("show");
                else {
                    $("#legalBlocks").tipsy("hide");
                }
            }
            function verifyAcdYr() {
                if (academicYear.value == "")
                    $("#acdYr").tipsy("show");
                else {
                    $("#acdYr").tipsy("hide");
                }
            }
        </script>
    </body>
</html>
