
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Login Page</title>
  <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>
  <link rel="stylesheet" href="css/style.css">
</head>
<% if(null != session.getAttribute("userName"))
{  
    if(session.getAttribute("role").equals("Admin"))
        response.sendRedirect("admin.jsp");  
    else 
        if(session.getAttribute("role").equals("Student"))
        response.sendRedirect("student.jsp"); 
    
    if (request.getSession(false)==null)
        request.getSession(true); 
}
%>

<body>
<div class="app-header-bar  centered">
  <div class="header content clearfix">
  <img alt="App" class="logo" src="images/mistletoe.ico">
  </div>
  </div>

  <div class="login-card">
  	<img src="images/outlet.png" align="">
    <h1>Login</h1><br>
  <form action ="LoginServlet">
    <input type="text" name="email" placeholder="Email">
    <input type="password" name="pass" placeholder="Password">
    <input type="submit" name="login" class="login login-submit" value="login">
  </form>

  <div class="login-help">
    <a href="registration.jsp">Register</a> &#183; <a href="#">Forgot Password</a>
  </div>
</div>
    
<!-- <div id="error"><img src="https://dl.dropboxusercontent.com/u/23299152/Delete-icon.png" /> Yr caps-lock is on.</div> -->

  <script src='http://codepen.io/assets/libs/fullpage/jquery_and_jqueryui.js'></script>

</body>

</html>