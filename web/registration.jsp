<%-- 
    Document   : registration
    Created on : Apr 23, 2015, 3:00:32 AM
    Author     : Crazzykid
--%>

<%@page import="cse308.School"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="org.hibernate.Query"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registration Page</title>
    <link rel="stylesheet" href="css/reg_style.css">
    <script type="text/javascript" src="./js/jquery.js"></script>
    <script type='text/javascript' src='./js/jquery.tipsy.js'></script>
    <link rel="stylesheet" href="./css/tipsy.css" type="text/css"/>

</head>
<body>
<div class="register-card">
    <h1>Register</h1><br>

    <form action="RegistrationServlet">
        <div id='forminputs'>
        <input type="text" rel="tipsy" id="fname" title="This field is required. Please enter your first name"
               name="fname" placeholder="First Name" onblur=verifyFirstName()>
        <input type="text" rel = "tipsy" id="lname" title="This field is required. Please enter your last name"
               name="lname" placeholder="Last Name" onblur=verifyLastName()>
        <input type="email" rel = "tipsy" id="email" title="This field is required. Please enter a valid email address"
               name="email" placeholder="Email" onblur=verifyEmail()>
        <input type="password" rel = "tipsy" id="password" title="This field is required. Please enter a password."
               name="pass" placeholder="Password" onblur=verifyPassword()>
        <input type="password" rel = "tipsy" id="passconf" title="The passwords you entered do not match."
               name="passconf" placeholder="Confirm Password" onblur=verifyConfirmPassword()>
        <br><br>
        <div class="genders" rel = "tipsy" id = "genders" title = "Wait! Did you forget to select your gender?">
            <input type="radio" name="sex" value="M" id="M">Male
            <input type="radio" name="sex" value="F" id="F">Female<br><br>
        </div><br>
        <select name="schoolselect" id="schoolselect" rel = "tipsy" title = "Wait! Did you forget to select a school?">
            <% 
                            Query query;
                            String school = "";
                            SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
                            Session openSession = sFactory.openSession();
                            Transaction openTransaction = openSession.beginTransaction();
                            String sql_query = "from School ";
                            query = openSession.createQuery(sql_query);
                            List list = query.list();
                            openSession.close();
                            School s = new School();
                            for (int i = 0; i < list.size(); i++) {
                                s = (School) list.get(i);
                                out.print("<option value=\"" + (s.getSchoolCode()) + "\">" + s.getSchoolName() + "</option>");
                            }
                      %>
        </select><br><br>
        </div>

        <input type="submit" name="register" class="register register-submit" value="Submit" onmouseover = "verifyGender();verifySchool()">
    </form>
</div>

<script type='text/javascript'>
    $(function() {
        $('input[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
        $('div[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
        $('select[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
    });

    var firstName = document.getElementById("fname");
    var lastName = document.getElementById("lname");
    var email = document.getElementById("email");
    var password = document.getElementById("password");
    var confirmPassword = document.getElementById("passconf");
    var male = document.getElementById("M");
    var female = document.getElementById("F");
    var schoolSelect = document.getElementById("schoolselect");
    var re = new RegExp(/(.+)@(.+){2,}\.(.+){2,}/);

    function verifyFirstName() {
        if (firstName.value == "") {
            $("#fname").tipsy("show");
        }
        else
            $("#fname").tipsy("hide");
    }
    function verifyLastName() {
        if (lastName.value == "")
            $("#lname").tipsy("show");

        else
            $("#lname").tipsy("hide");
    }
    function verifyEmail() {
        if (re.test(email.value) == false) {
            $("#email").tipsy("show");
        }
        else {
            $("#email").tipsy("hide");
        }
    }
    function verifyPassword() {
        if (password.value == "")
            $("#password").tipsy("show");
        else {
            $("#password").tipsy("hide");
        }
    }
    function verifyConfirmPassword() {
        if ((!password.value.match(confirmPassword.value)) || confirmPassword.value == "") {
            $("#passconf").tipsy("show");
        }
        else
            $("#passconf").tipsy("hide");
    }
    function verifyGender(){

        if((!male.checked) && (!female.checked))
            $("#genders").tipsy("show");
        else
            $("#genders").tipsy("hide");
    }
    function verifySchool(){
        if(schoolSelect.value=="None")
            $("#schoolselect").tipsy("show");
        else
            $("#schoolselect").tipsy("hide");
    }


</script>
</body>
</html>