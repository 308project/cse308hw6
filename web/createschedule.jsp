<%-- 
    Document   : addsemesters
    Created on : Apr 22, 2015, 8:35:26 PM
    Author     : ShawnCruz
--%>

<%@page import="cse308.AcademicYear"%>
<%@page import="cse308.School"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="cse308.Student"%>
<%@page import="cse308.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.Query"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Create Schedule Page</title>
        <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>
        <link rel="stylesheet" href="css/reg_style.css">
        <script type="text/javascript" src="./js/jquery.js"></script>
        <script type='text/javascript' src='./js/jquery.tipsy.js'></script>
        <link rel="stylesheet" href="./css/tipsy.css" type="text/css"/>
    </head>
    
    <body>

        <div class="register-card">
            <form action="#">
                <select name="academicYr" id="academicYr" rel = "tipsy" title = "Wait! Did you forget to select a school?">
                    <option value="None">Select Academic Year</option>
                    <%
                        String email = (String) session.getAttribute("userName");
                        String query = "from AcademicYear a, Student s where s.emailAddress = '" + email + "' AND"
                                + "a.schoolCode = s.schoolCode";
                        SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
                        Session openSession = sFactory.openSession();
                        Transaction openTransaction = openSession.beginTransaction();
                        Query q = openSession.createQuery(query);
                        ArrayList<Object[]> list = new ArrayList<Object[]>();
                        list = (ArrayList<Object[]>) q.list();
                        openSession.close();
                        AcademicYear academicYear = new AcademicYear();
                        for(Object[] tableItem: list) {
                            academicYear = (AcademicYear)tableItem[0];
                            out.println("<option value='" + academicYear.getId().toString() + "'>"+academicYear.getId().getYear()+"</option>");
                        }
                    %>
                </select>
                <hr/>
                <h3>Add Course/Section:</h3>
                <input type="text" rel="tipsy" name="courseName" id="courseName" placeholder="Course Name" onblur="#" title = "This field is required. Please enter the number of semesters">
                <select name="semesterRange" id="semesterRange" rel = "tipsy" title = "Wait! Did you forget to select a school?">
                    <option value="None">Select Semester Range</option>
                    <%
                        //INSERT W/ DB VALUES FOR ACADEMIC YEARS
                    %>
                </select>
                <select name="scheduleBlock" id="scheduleBlock" rel = "tipsy" title = "Wait! Did you forget to select a school?">
                    <option value="None">Select Schedule Block</option>
                    <%
                        //INSERT W/ DB VALUES FOR ACADEMIC YEARS
                    %>
                </select>
                
                <br><br>
                <input type="submit" name="addClass" class="register register-submit" value="Submit">
            </form>
        </div>
        <script type='text/javascript'>
            $(function() {
                $('select[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
                $('input[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
                });
            
            var schoolSelect = document.getElementById("schoolSelect");
            var numberSemesters = document.getElementById("numSem");
            var numberDays = document.getElementById("numDays");
            var numberPer = document.getElementById("numPer");
            var rangeLunch = document.getElementById("rangeLunc");
            var legalBlocks = document.getElementById("legalBlocks");
            var academicYear = document.getElementById("acdYr");
            
            function verifySchool(){
                if(schoolSelect.value=="None")
                    $("#schoolSelect").tipsy("show");
                else
                     $("#schoolSelect").tipsy("hide");
            }
            function verifyNumSemesters() {
                if (numberSemesters.value == "") {
                    $("#numSem").tipsy("show");
                }
                else
                    $("#numSem").tipsy("hide");
            }
            function verifyNumDays() {
                if (numberDays.value == "")
                    $("#numDays").tipsy("show");
                else
                    $("#numDays").tipsy("hide");
            }
            function verifyNumPer() {
               if (numberPer.value == "")
                    $("#numPer").tipsy("show");
                else
                    $("#numPer").tipsy("hide");
            }
            function verifyRangeLunc() {
                if (rangeLunch.value == "")
                    $("#rangeLunc").tipsy("show");
                else {
                    $("#rangeLunc").tipsy("hide");
                }
            }
            function verifyLegalBlocks() {
                if (legalBlocks.value == "")
                    $("#legalBlocks").tipsy("show");
                else {
                    $("#legalBlocks").tipsy("hide");
                }
            }
            function verifyAcdYr() {
                if (academicYr.value == "")
                    $("#academicYr").tipsy("show");
                else {
                    $("#academicYr").tipsy("hide");
                }
            }
        </script>
    </body>
</html>


