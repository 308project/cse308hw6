<%-- 
    Document   : admin
    Created on : Apr 2, 2015, 2:05:15 AM
    Author     : lamar
--%>

<%@page import="cse308.EmailVerification"%>
<%@page import="cse308.PlannerMethods"%>
<%@page import="cse308.School"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="cse308.Student"%>
<%@page import="cse308.User"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Query"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="dr" uri="/WEB-INF/tlds/displayRequest"%>

<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Administrator Page</title>

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/adminStyle.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet"
              type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <%     //Verifying if the User is Logged in 
        if (null == session.getAttribute("userName")) {
            response.sendRedirect("index.jsp");
        }
    %>
    <%
            String email = (String) session.getAttribute("userName");
            List school = PlannerMethods.getSearchList("school");
            List student = PlannerMethods.getSearchList("student");
    %>


    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

        <!-- Navigation -->
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top"> 
                        <span class="light">Welcome,</span> <%  out.print(PlannerMethods.getUserList().get(email));  %>
                    </a>
                </div>
                <!-- Collect the nav links, forms, and other content toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li>
                            <a class="page-scroll" href="./addacademicyear.jsp">Add Academic Year</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#" data-toggle="modal" data-target="#add-school">Add School</a>
                        </li>
                        <li>
                            <a href="#" class="btn-toolbar" type="button" id="dropdown" data-toggle="dropdown">
                                Requests
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                                      <dr:request>
                                       <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                        <span class="glyphicon glyphicon-user"></span> ${lName}, ${fName}
                                        <div style="float: right;">
                                        <span class="glyphicon glyphicon-plus add" onclick="approveRequest('${email}')"></span>
                                        <span class="glyphicon glyphicon-remove del" onclick="deleteRequest('${email}')"></span>
                                        </div>
                                         </a></li>
                                   </dr:request>
                                     
                                <li role="presentation" class="divider"></li>
                                <li class="all" role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                        <span class="glyphicon glyphicon-ok" onclick="href='ApprovedAllRequestServlet' "></span> Approve All</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="page-scroll" href="logout.jsp">Logout</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Home</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

         <!-- Intro Header -->
        <header class="intro">
            <div class="intro-body">
                <div class="container">
                </div>
            </div>
        </header>

        <div class="search">
            <div class="input-group">
                <div class="input-group-btn search-panel">
                    <button type="button" class="btn btn-default dropdown-toggle"
                            data-toggle="dropdown">
                        <span id="search_concept">Filter by</span> <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" onclick="getFilter()">
                        <li><a href="#Student">Student</a></li>
                        <li><a href="#School">School</a></li>
                    </ul>
                </div>
                <input type="hidden" name="search_param" value="all" id="search_param">
                <input type="text" onkeyup="autoComplete()" id="searchBar" class="form-control"
                       name="x" placeholder="Search term...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span>
                    </button>
                </span>
            </div>
        </div>
        <form action="AddSchoolServlet" >
            <!-- Add School Modal -->
            <div class="modal fade" id="add-school">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add School</h4>
                        </div>
                        <div class="modal-body" id="add-school-body">

                            <input name="school" type="text" placeholder = "School Name" id="add-school-name">
                              <input name="zip" type="text" placeholder = "Zip Code" id="add-school-zipcode">

                        </div>
                        <div class="modal-footer">
                            <div class="add-school-button">
                                <button type="button" class="btn btn-default" data-toggle="modal" data-target="#confirm-add-school" id = "add-school-2">
                                    <span class="glyphicon glyphicon-plus"></span> Add School
                                </button>
                            </div>
                            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->

            <!-- Add School Modal -->
            <div class="modal fade" id="confirm-add-school">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add School</h4>
                        </div>
                        <div class="modal-body" id="confirm-add-body">
                        </div>
                        <div class="modal-footer">
                            <div class="add-school-button">
                                <div class="add-school-button">
                                    <button type="submit" class="btn btn-default" id="confirm-school">
                                        Yes <span class="glyphicon glyphicon-ok"></span></button>

                                    <button type="button" class="btn btn-default" id="abandon-school"  data-dismiss="modal">
                                        No <span class="glyphicon glyphicon-remove"></span></button>
                                </div>
                            </div>
                            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        </form>
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/grayscale.js"></script>

        <!-- Search Bar -->
        <script src="js/searchbar.js"></script>

        <!-- Jquery libraries for autocomplete -->
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <script>
            
            function approveRequest(email){
                    var xmlhttp;
                            //console.log(email);
                            if (email == "") {
                                return;
                            }
                            //SENDS THE CONFIRMATION EMAIL
                           
                            //showScheduleBlock(str);
                            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                                xmlhttp = new XMLHttpRequest();
                            }
                            else {// code for IE6, IE5
                                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            xmlhttp.onreadystatechange = function() {

                                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                    alert("Account has been approved");
                                    location.reload();
                                }
                            }
                            xmlhttp.open("GET", "ApprovedAccountServlet?email=" + email, true);
                            xmlhttp.send();
            }
            
            function deleteRequest(email){
                    var xmlhttp;
                            //console.log(email);
                            if (email == "") {
                                return;
                            }
                            //SENDS THE CONFIRMATION EMAIL
                           
                            //showScheduleBlock(str);
                            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                                xmlhttp = new XMLHttpRequest();
                            }
                            else {// code for IE6, IE5
                                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                            }
                            xmlhttp.onreadystatechange = function() {

                                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                    alert("Account has been deleted");
                                    location.reload();
                                }
                            }
                            xmlhttp.open("GET", "DeleteAccountServlet?email=" + email, true);
                            xmlhttp.send();
            }
                    //------DELETE Students------
                    function deleteStudent(e){
                        var x = e.clientX, y = e.clientY;
                        var  elementMouseIsOver = document.elementFromPoint(x, y);
                        var studentToDelete = ($(elementMouseIsOver).parent().siblings().html());
                        var email = studentToDelete.split("[");
                        email = email[1];
                        email = email.replace("]","");
                        console.log(email);
                        
                        location.replace("DeleteAccountServlet?email="+email);                  
                    }
                    
                    function deleteSchool(e){
                        var x = e.clientX, y = e.clientY;
                        var  elementMouseIsOver = document.elementFromPoint(x, y);
                        var schoolToDelete = ($(elementMouseIsOver).parent().siblings().html());
                        var code = schoolToDelete.split("[");
                        var schoolCode = code;
                        code = code[1];
                        code = code.replace("]","");
                        var name = code + schoolCode[0];
              
                        console.log("name is : "+ name);
                     
                         location.replace("DeleteSchoolServlet?name="+name);                       
                    }


                    $(".all").click(function() {
                        window.location.replace("ApprovedAllRequestServlet");
                    });

                    //------MODAL HANDLERS------
                    $("#add-school-2").click(function() {
                        var schoolName = $("#add-school-name").val();
                        console.log(schoolName);
                        $("#confirm-add-body").html("Would you like to add " + schoolName + "?");
                    });

                    //-----AUTOCOMPLETE------
                    var delStudent = '<span onclick = "deleteStudent(event)" class="glyphicon glyphicon-remove deleteStudent" style="float: right;"></span>'; 
                    var delSchool = '<span onclick = "deleteSchool(event)" class="glyphicon glyphicon-remove deleteStudent" style="float: right;"></span>';
                    var option = "";
                    var notFound = false; //whether or not the search returned any results
                    
                    /* this allows us to pass in HTML tags to autocomplete. Without this they get escaped */
                    $[ "ui" ][ "autocomplete" ].prototype["_renderItem"] = function( ul, item) {
                     if(!notFound){
                         if(option == "Student"){
                            return $( "<li></li>" ) 
                           .data( "item.autocomplete", item )
                           .append( $( "<a></a>" ).html( item.label ))
                            .append( $( "<a>"+delStudent+"</a>" ))
                           .appendTo( ul );
                         }
                         else if(option == "School"){
                            return $( "<li></li>" ) 
                           .data( "item.autocomplete", item )
                           .append( $( "<a></a>" ).html( item.label ))
                            .append( $( "<a>"+delSchool+"</a>" ))
                           .appendTo( ul );
                         }
                    else{
                     return $( "<li></li>" ) 
                    .data( "item.autocomplete", item )
                    .append( $( "<a></a>" ).html( item.label ))
                    .appendTo( ul );
                    }
                    }
                     else{
                     return $( "<li></li>" ) 
                    .data( "item.autocomplete", item )
                    .append( $( "<a></a>" ).html( item.label ))
                    .appendTo( ul );
                    }
                       };
                        

                    //Use these lists if the appropriate filter is chosen
                    var studentList = [<% for (int i = 0; i < student.size(); i++) { %>"<%= student.get(i) %>"<%= i + 1 < student.size() ? ",":"" %><% } %>];
                    var schoolList = [<% for (int i = 0; i < school.size(); i++) { %>"<%= school.get(i) %>"<%= i + 1 < school.size() ? ",":"" %><% } %>];
                    //If no filter is chosen, use one big list
                    var masterList = studentList.concat(schoolList);
                    //Function for updating filter when a filter is selected
                    function getFilter() {
                        option = document.getElementById("search_concept").innerHTML;
                    }
                    //Autocomplete function
                    function autoComplete() {
                        var searchTerm = $("#searchBar").val();//stores search term
                        var noResult; //stores error string                       
                        if (option == "School") {
                            noResult = {value: "", label: "The school " + searchTerm + " was not found."};
                            $("#searchBar").autocomplete({
                                source: schoolList,
                                response: function(event, ui) {
                                    if (ui.content.length === 0) {
                                        ui.content.push(noResult);
                                        notFound = true;
                                    }
                                    else
                                        notFound = false;
                                },
                            });
                        }
                        else if (option == "Student") {
                                                     
                            noResult = {value: "", label: "The student " + searchTerm + " was not found."};
                            $("#searchBar").autocomplete({
                                source: studentList,
                                response: function(event, ui) {
                                    if (ui.content.length === 0){
                                        notFound = true;
                                        ui.content.push(noResult);
                                    }                                         
                                    else
                                        notFound = false;
                                                               
                                },
                            });
                        }
                        else {
                            noResult = {value: "", label: "No results found."};
                            $("#searchBar").autocomplete({
                                source: masterList,
                                response: function(event, ui) {
                                    if (ui.content.length === 0) {
                                        ui.content.push(noResult);
                                    }
                                }
                            });
                        }
                    }

        </script>

    </body>

</html>
