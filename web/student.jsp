<%@page import="java.util.Calendar"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="cse308.Section"%>
<%@page import="cse308.ScheduleBlock"%>
<%@page import="cse308.AcademicYear"%>
<%@page import="cse308.PlannerMethods" %>
<%@page import="cse308.Friends" %>
<%@page import="java.util.HashMap" %>
<%@page import="cse308.FriendRequestId" %>
<%@page import="java.util.ArrayList" %>
<%@page import="cse308.FriendRequest" %>
<%@page import="cse308.User" %>
<%@page import="java.util.List" %>
<%@page import="org.hibernate.Transaction" %>
<%@page import="org.hibernate.cfg.Configuration" %>
<%@page import="org.hibernate.SessionFactory" %>
<%@page import="org.hibernate.Session" %>
<%@page import="org.hibernate.Query" %>
<%@taglib prefix="fr" uri="/WEB-INF/tlds/Friends" %>
<%@taglib prefix="sec" uri="/WEB-INF/tlds/courselist" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!--Possibly Include Student's Name-->
        <title>Student Page</title>

        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="css/studentStyle.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet"
              type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
        
         <link href="./css/multi-select.css" media="screen" rel="stylesheet" type="text/css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="js/jquery.easing.min.js"></script>

        <!-- Custom Theme JavaScript -->
        <script src="js/grayscale.js"></script>

        <!-- Search Bar -->
        <script src="js/searchbar.js"></script>
        <!-- Jquery multiselect -->
        <script src="./js/jquery.multi-select.js" type="text/javascript"></script>


    </head>
    <%     //Verifying if the User is Logged in
        if (null == session.getAttribute("userName")) {
            response.sendRedirect("index.jsp");
        }
    %>
    <%
        String email = (String) session.getAttribute("userName");
    %>

    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

        <!-- Navigation -->
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                        <i class="fa fa-bars"></i>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top">
                        <span class="light">Welcome, <%out.print("<strong>"+PlannerMethods.getUserList().get(email)+"</strong>");%></span>
                    </a>

                </div>


                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-right navbar-main-collapse">

                    <ul class="nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->

                        <li class="hidden">
                            <a href="#page-top"></a>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Friends
                                <span class="glyphicon glyphicon-chevron-down"></span>
                                </a>
                            <ul class="dropdown-menu" role="menu" id = "friendList">
                                <fr:FriendsTagHandler userEmail="${userName}">
                                    ${friend}
                                </fr:FriendsTagHandler>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Courses
                            <span class="glyphicon glyphicon-chevron-down"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu" id = "couresList">
                                <!--DISPLAYS ALL COURSES FOR STUDENT'S SCHOOL-->
                                <sec:CourseCatalogHandler userEmail="${userName}">
                                    ${courseElement}
                                </sec:CourseCatalogHandler>
                             </ul>
                        </li>

                        <li>

                            <a href="#" class="btn-toolbar" type="button" id="dropdown" data-toggle="dropdown">
                                Requests
                                <span class="glyphicon glyphicon-chevron-down"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu" id="friendRequestList">
                                <fr:FriendRequestTagHandler userEmail="${userName}">
                                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">
                                        <span class="glyphicon glyphicon-user"></span> ${name}
                                        <div style="float: right;">
                                        <span class="glyphicon glyphicon-plus add" onclick="approveRequest('${queryString}')"></span>
                                        <span class="glyphicon glyphicon-remove del" onclick="deleteRequest('${queryString}')"></span>
                                        </div>
                                         </a></li>                              
                                                               
                                </fr:FriendRequestTagHandler>

                            </ul>
                        </li>
                        <li>
                            <a class="page-scroll" href="logout.jsp">Logout</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Home</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
        </nav>

        <!-- Intro Header -->
        <!--<header class="intro">-->

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="search">
                        <div class="col-xs-8 col-xs-offset-2">
                             <form action="FriendRequestServlet">
                            <div class="input-group">
                                <div class="input-group-btn search-panel">
                                    <button type="button" class="btn btn-default dropdown-toggle">
                                        <span id="search_concept">Request Friend</span>

                                    </button>

                                </div>
                                <input type="hidden" name="search_param" value="all" id="search_param">
                                <input type="text" class="form-control" name="friend" placeholder="(e.g: user@mail.com)">
                                <span class="input-group-btn">
                                   
                                    <submit class="btn btn-default" type="button" ><span class="glyphicon glyphicon-search"></span>
                                    </submit>
                                    
                                </span>
                            </div>
                             </form>
                        </div>
                    </div>


                    <div class="container2"></div>
                    <div class="schedule">

                        <a href data-toggle="modal" data-target="#assigned">
                            <img src="images/calendar_assigned.png">
                        </a>
                        <a href data-toggle="modal" data-target="#desired">
                            <img src="images/calendar_desired.png">
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!--</header>-->

        <!-- Assigned Schedule Modal -->
        <div class="modal fade" id="assigned">
            <div class="modal-dialog" id="assigned-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="assigned-title">Assigned Schedule</h4>
                    </div>
                    <div class="modal-body" id="assigned-body"></div>
                    <div class="modal-footer">
                        <div class="sched-controls">
                            <button type="button" class="btn btn-default" id="a-toggle">
                                Toggle <span class="glyphicon glyphicon-calendar"></span></button>
                            <button type="button" class="btn btn-default" id="a-edit" data-toggle="modal" data-target="#edit">
                                Edit <span class="glyphicon glyphicon-edit"></span></button>
                            <button type="button" class="btn btn-default" id="a-export" data-toggle="modal" data-target="#export">
                                Export <span class="glyphicon glyphicon-share"></span></button>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Desired Schedule Modal -->
        <div class="modal fade" id="desired">
            <div class="modal-dialog" id="desired-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="desired-title">Desired Schedule</h4>
                    </div>
                    <div class="modal-body" id="desired-body"></div>
                    <div class="modal-footer">
                        <div class="sched-controls">
                            <button type="button" class="btn btn-default" id="d-edit" data-toggle="modal" data-target="#desired-edit">
                                Edit <span class="glyphicon glyphicon-edit"></span></button>
                            <button type="button" class="btn btn-default" id="d-export" data-toggle="modal" data-target="#export">
                                Export <span class="glyphicon glyphicon-share"></span></button>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!----Edit Assigned Schedule Modal----->
        <div class="modal fade" id="edit">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title">Edit Assigned Schedule</h3>
                    </div>
                    <div class="modal-body" id="edit-body">
                        <form action="AddCourseServlet">
                            <select name="academicYr" id="academicYr" onchange="showSemesters(this.value)" placeholder = "Select Academic Year">
                                <option value="None">Select Academic Year</option>
                                <%
                                
                                    
                                    //String email = "sample"; //(String) session.getAttribute("userName");
                                    String query = "from AcademicYear a, Student s where s.emailAddress = '" + email + "' AND "
                                            + "a.id.schoolCode = s.schoolCode";
                                    SessionFactory sFactory = new Configuration().configure().buildSessionFactory();
                                    Session openSession = sFactory.openSession();
                                    Transaction openTransaction = openSession.beginTransaction();
                                    Query q = openSession.createQuery(query);
                                    ArrayList<Object[]> list = new ArrayList<Object[]>();
                                    list = (ArrayList<Object[]>) q.list();
                                    openSession.close();
                                    AcademicYear academicYear = new AcademicYear();
                                    for (Object[] tableItem : list) {
                                        academicYear = (AcademicYear) tableItem[0];
                                        out.println("<option value='" + academicYear.getId().toString() + "'>" + academicYear.getId().getYear() + "</option>");
                                    }
                                %>
                            </select>
                            <hr/>
                            <h4>Add Course/Section:</h4>
                            <p id="noCourse"></p>
                            <input type="text" rel="tipsy" name="courseCode" id="courseCode" placeholder="Course Code" onblur = "checkCourse(this.value)" onkeyup="clearData()"><br>
                            <input type="text" rel="tipsy" name="courseName" id="courseName" placeholder="Course Name"><br>
                            <select name="courseSection" id="courseSection" placeholder="Section" onchange="changeInstructor()">
                                <option value="None">Select Section</option>
                            </select>
                            <input type="text" rel="tipsy" name="courseInstructor" id="courseInstructor" placeholder="Instructor"><br>
                            <select name="semester" id="semester" rel = "tipsy">
                                <option value="None">Select Semester</option>

                            </select><br>
                            <input type="text" rel="tipsy" name="scheduleBlock" id="scheduleBlock" placeholder="Select schedule Block"><br>    
                                <input type ="hidden" name ="scheduleBlock" id="scheduleBlockHidden">
                                <input type ="hidden" name ="email" id="email">
                                <input type ="hidden" name="unfriend" id="unfriend" value="${userName}">
                           </select>
                           <hr>
                           <span id="addSection"></span>
                           <br><br>
                            <input type="submit" name="addClass" class="btn btn-default" value="Submit">
                            
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

                            <!----Edit Desired Schedule Modal----->
                            <div class="modal fade" id="desired-edit">
                                <div class="modal-dialog" id ="desired-edit-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                    aria-hidden="true">&times;</span></button>
                                            <h3 class="modal-title">Edit Desired Schedule</h3>
                                        </div>
                                        <div class="modal-body" id="desired-edit-body" align = "center">
                                            <form action="DesiredCourseServlet">   
                                                <br>
                                                <p>Filter Generate Schedule By:</p>
                                                <select name ="preferences" id ="chooseSchedulePreferences">
                                                    <option value="">Select Option</option>
                                                    <option value="withFriends">With Friends</option>
                                                    <option value="withPrefProf">With Preferred Professors</option>
                                                    <option value="conflictFree">By Conflict Free</option>
                                                </select>
                                                <hr>
                                               <select multiple="multiple" id="desired-course-select" name="my-select[]">
                                               
                                            </select>
                                                <p></p><input type="submit" name="addClass" class="btn btn-default" value="Submit">

                                            </form>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>


    <!----Export Schedule Modal----->
        <div class="modal fade" id="export">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Export Schedule</h4>
                    </div>
                    <div class="modal-body" id="export-body">
                        <label>Filename: </label> <input type="text" id="filename" name = "filename" placeholder = "e.g
                                                         'MySchedule'">
                        <div class = "export-options"><br>
                            <button type="button" class="btn btn-default" id="download">Download
                                <span class = "glyphicon glyphicon-save"></span>
                            </button>
                        </div>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <!---Add Course Modal----->
        <div class="modal fade" id="add-course">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title">Edit Schedule</h3>
                       
                    </div>
                    <div class="modal-body" id="edit-body">
                        <form action="AddCourseServlet">
                            <hr/>
                            <h4>Add Course/Section:</h4>
                            <input type="text" rel="tipsy" name="courseName" id="addCourseName" placeholder="Enter Course Name"><br>
                            <input type="text" rel="tipsy" name="courseInstructor" id="addCourseInstructor" placeholder="Enter Instructor"><br>
                             <input type="text" rel="tipsy" name="courseSection" id="addCourseSection" placeholder="Enter Section"><br>

                             <input type="hidden" rel="tipsy" name="academicYr" id="academic" ><br>
                             <input type="hidden" rel="tipsy" name="courseCode" id="course" >
                             Select Semester Range: <br>
                             <select multiple name="semesterRange[]">
                                 <%
                                    int thisYear = Calendar.getInstance().get(Calendar.YEAR);
                                    AcademicYear aYear = null;
                                    for (Object[] tableItem : list) {
                                        academicYear = (AcademicYear) tableItem[0];
                                        if (academicYear.getId().getYear() == thisYear)
                                            aYear = academicYear;
                                    }
                                    int numSemesters = 0;
                                    if (aYear != null)
                                        numSemesters = aYear.getNumSemesters();
                                    for (int i = 0; i < numSemesters; i++)
                                        out.println("<option value='"+(i+1)+"'>Semester " + (i + 1) + "</option>");
                                    %>
                             </select>
                             <input type="text" rel="tipsy" name="semester" id="addSemester" placeholder="Enter Semester" onchange="showScheduleBlockNew()"><br>
                             <select name ="scheduleBlock" id ="addScheduleBlock">
                             <option>Select Schedule Block</option>
                             </select>
                            <input type="submit" name="addNewClass" class="btn btn-default" value="Submit">
                            
                        </form>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->

        <div id = "assigned-schedule" style="display: none;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Period</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td id="a11"></td>
                        <td id="a12"></td>
                        <td id="a13"></td>
                        <td id="a14">  </td>
                        <td id="a15">  </td>
                    </tr>
                <th scope="row">2</th>
                <td id="a21">  </td>
                <td id="a22">  </td>
                <td id="a23">  </td>
                <td id="a24">  </td>
                <td id="a25">  </td>
                </tr>
                <tr>
                    <th scope="row">3</th>
                    <td id="a31">  </td>
                    <td id="a32">  </td>
                    <td id="a33">  </td>
                    <td id="a34">  </td>
                    <td id="a35">  </td>
                </tr>
                <tr>
                    <th scope="row">4</th>
                    <td id="a41">  </td>
                    <td id="a42">  </td>
                    <td id="a43">  </td>
                    <td id="a44">  </td>
                    <td id="a45">  </td>
                </tr>
                <tr>
                    <th scope="row">5</th>
                    <td id="a51">  </td>
                    <td id="a52">  </td>
                    <td id="a53">  </td>
                    <td id="a54">  </td>
                    <td id="a55">  </td>
                </tr>
                <tr>
                    <th scope="row">6</th>
                    <td id="a61">  </td>
                    <td id="a62">  </td>
                    <td id="a63">  </td>
                    <td id="a64">  </td>
                    <td id="a65">  </td>
                </tr>
                <tr>
                    <th scope="row">7</th>
                    <td id="a71">  </td>
                    <td id="a72">  </td>
                    <td id="a73">  </td>
                    <td id="a74">  </td>
                    <td id="a75">  </td>
                </tr>
                <tr>
                    <th scope="row">8</th>
                    <td id="a81">  </td>
                    <td id="a82">  </td>
                    <td id="a83">  </td>
                    <td id="a84">  </td>
                    <td id="a85">  </td>
                </tr>
                <tr>
                    <th scope="row">9</th>
                    <td id="a91">  </td>
                    <td id="a92">  </td>
                    <td id="a93">  </td>
                    <td id="a94">  </td>
                    <td id="a95">  </td>
                </tr>
                <tr>
                    <th scope="row">10</th>
                    <td id="a101">  </td>
                    <td id="a102">  </td>
                    <td id="a103">  </td>
                    <td id="a104">  </td>
                    <td id="a105">  </td>
                </tr>
                <tr>
                    <th scope="row">11</th>
                    <td id="a111">  </td>
                    <td id="a112">  </td>
                    <td id="a113">  </td>
                    <td id="a114">  </td>
                    <td id="a115">  </td>
                </tr>
                <tr>
                    <th scope="row">12</th>
                    <td id="121">  </td>
                    <td id="122">  </td>
                    <td id="123">  </td>
                    <td id="124">  </td>
                    <td id="125">  </td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id = "desired-schedule" style="display: none;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Period</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td id="d11">  </td>
                        <td id="d12">  </td>
                        <td id="d13">  </td>
                        <td id="d14">  </td>
                        <td id="d15">  </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td id="d21">  </td>
                        <td id="d22">  </td>
                        <td id="d23">  </td>
                        <td id="d24">  </td>
                        <td id="d25">  </td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td id="d31">  </td>
                        <td id="d32">  </td>
                        <td id="d33">  </td>
                        <td id="d34">  </td>
                        <td id="d35">  </td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td id="d41">  </td>
                        <td id="d42">  </td>
                        <td id="d43">  </td>
                        <td id="d44">  </td>
                        <td id="d45">  </td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td id="d51">  </td>
                        <td id="d52">  </td>
                        <td id="d53">  </td>
                        <td id="d54">  </td>
                        <td id="d55">  </td>
                    </tr>
                    <tr>
                        <th scope="row">6</th>
                        <td id="d61">  </td>
                        <td id="d62">  </td>
                        <td id="d63">  </td>
                        <td id="d64">  </td>
                        <td id="d65">  </td>
                    </tr>
                    <tr>
                        <th scope="row">7</th>
                        <td id="d71">  </td>
                        <td id="d72">  </td>
                        <td id="d73">  </td>
                        <td id="d74">  </td>
                        <td id="d75">  </td>
                    </tr>
                    <tr>
                        <th scope="row">8</th>
                        <td id="d81">  </td>
                        <td id="d82">  </td>
                        <td id="d83">  </td>
                        <td id="d84">  </td>
                        <td id="d85">  </td>
                    </tr>
                    <tr>
                        <th scope="row">9</th>
                        <td id="d91">  </td>
                        <td id="d92">  </td>
                        <td id="d93">  </td>
                        <td id="d94">  </td>
                        <td id="d95">  </td>
                    </tr>
                    <tr>
                        <th scope="row">10</th>
                        <td id="d101">  </td>
                        <td id="d102">  </td>
                        <td id="d103">  </td>
                        <td id="d104">  </td>
                        <td id="d105">  </td>
                    </tr>
                    <tr>
                        <th scope="row">11</th>
                        <td id="d111">  </td>
                        <td id="d112">  </td>
                        <td id="d113">  </td>
                        <td id="d114">  </td>
                        <td id="d115">  </td>
                    </tr>
                    <tr>
                        <th scope="row">12</th>
                        <td id="d121">  </td>
                        <td id="d122">  </td>
                        <td id="d123">  </td>
                        <td id="d124">  </td>
                        <td id="d125">  </td>
                    </tr>
                </tbody>
            </table>
        </div>
            
            <div id = "friends-schedule" style="display: none;">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Period</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td id="f11">  </td>
                        <td id="f12">  </td>
                        <td id="f13">  </td>
                        <td id="f14">  </td>
                        <td id="f15">  </td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td id="f21">  </td>
                        <td id="f22">  </td>
                        <td id="f23">  </td>
                        <td id="f24">  </td>
                        <td id="f25">  </td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td id="f31">  </td>
                        <td id="f32">  </td>
                        <td id="f33">  </td>
                        <td id="f34">  </td>
                        <td id="f35">  </td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td id="f41">  </td>
                        <td id="f42">  </td>
                        <td id="f43">  </td>
                        <td id="f44">  </td>
                        <td id="f45">  </td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td id="f51">  </td>
                        <td id="f52">  </td>
                        <td id="f53">  </td>
                        <td id="f54">  </td>
                        <td id="f55">  </td>
                    </tr>
                    <tr>
                        <th scope="row">6</th>
                        <td id="f61">  </td>
                        <td id="f62">  </td>
                        <td id="f63">  </td>
                        <td id="f64">  </td>
                        <td id="f65">  </td>
                    </tr>
                    <tr>
                        <th scope="row">7</th>
                        <td id="f71">  </td>
                        <td id="f72">  </td>
                        <td id="f73">  </td>
                        <td id="f74">  </td>
                        <td id="f75">  </td>
                    </tr>
                    <tr>
                        <th scope="row">8</th>
                        <td id="f81">  </td>
                        <td id="f82">  </td>
                        <td id="f83">  </td>
                        <td id="f84">  </td>
                        <td id="f85">  </td>
                    </tr>
                    <tr>
                        <th scope="row">9</th>
                        <td id="f91">  </td>
                        <td id="f92">  </td>
                        <td id="f93">  </td>
                        <td id="f94">  </td>
                        <td id="f95">  </td>
                    </tr>
                    <tr>
                        <th scope="row">10</th>
                        <td id="f101">  </td>
                        <td id="f102">  </td>
                        <td id="f103">  </td>
                        <td id="f104">  </td>
                        <td id="f105">  </td>
                    </tr>
                    <tr>
                        <th scope="row">11</th>
                        <td id="f111">  </td>
                        <td id="f112">  </td>
                        <td id="f113">  </td>
                        <td id="f114">  </td>
                        <td id="f115">  </td>
                    </tr>
                    <tr>
                        <th scope="row">12</th>
                        <td id="f121">  </td>
                        <td id="f122">  </td>
                        <td id="f123">  </td>
                        <td id="f124">  </td>
                        <td id="f125">  </td>
                    </tr>
                </tbody>
            </table>
        </div>


          <%    
                              int  year = Calendar.getInstance().get(Calendar.YEAR);
                                    
                SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
                openSession = sessionFactory.openSession();
                Transaction transaction = null;
                String query_sql = "from ScheduleBlock S where S.id.emailAddress = '" + email + "' AND  S.id.year = '"+year+"' AND S.id.role = 'Assigned' ";
                String querySchool = "from AcademicYear A, Student S where S.emailAddress = '" + email + "' AND  A.id.year = '"+year+"' AND S.schoolCode = A.id.schoolCode";
                String querySection = "from Section";
                Query sectionQuery = openSession.createQuery(querySection);
                Query querysql = openSession.createQuery(query_sql);
                Query query_school = openSession.createQuery(querySchool);
                
                
            ArrayList<Object[]> academicList = new ArrayList<Object[]>();
            academicList = (ArrayList<Object[]>) query_school.list();
            //openSession.close();
          int numCycle = 0;
            //Displaying the result of the DB in the Request Tag on the admin's page
            // for (int i = 0; i < studentList.size(); i++) {
            for (Object[] tableItem : academicList) {
               AcademicYear a = (AcademicYear) tableItem[0];
                numCycle = Integer.parseInt(a.getScheduleCycle());
                
            }
                
                List schedule = querysql.list();
                List section = sectionQuery.list();
                int scheduleYear;
                String semester ="";
                String timeSlot = "";
                int sectionId;
                 String courseName = "";
                  String courseCode = "";
                  int sectionNumber;
                  String scheduleBlock = "";
                for(int i=0; i<schedule.size(); i++){          
                    ScheduleBlock s = (ScheduleBlock)schedule.get(i);
                        scheduleYear = s.getId().getYear();
                        semester = s.getId().getSemester();
                         timeSlot = s.getId().getTimeSlot();
                         sectionId = s.getId().getSectionId();
                    for(int k=0; k<section.size(); k++){
                        Section sec = (Section)section.get(k);
                        if(sectionId == sec.getId()){
                             courseName = sec.getCourseName();
                             courseCode = sec.getCourseCode();
                             sectionNumber = sec.getSectionNumber();
                            // scheduleBlock = sec.getScheduleBlock();
                             String[] splitScheduleBlock = timeSlot.split(":");
                             String period = splitScheduleBlock[0];
                             String numDays = splitScheduleBlock[1];
                             String[] daysArray = numDays.split(";");
                             System.out.print("Days array"+daysArray.length);
                              System.out.print("period "+period);
                               System.out.print("numDays"+daysArray); 
                
                           
                          out.print("<script>");
                            for(int j=0;j<daysArray.length; j++)
                             {
                                String[] cellArray = new String[daysArray.length];
                                 String[] cellArray2 = new String[daysArray.length];
                                  String[] cellArray3 = new String[daysArray.length];
                            
                                 String str = "$(\"#";
                               cellArray[j] = "a" + period + daysArray[j];
                                cellArray2[j] = "a"+ period + (Integer.parseInt(daysArray[j]) +numCycle);
                                cellArray3[j] = "a"+ period + (Integer.parseInt(daysArray[j]) +numCycle+numCycle);
                                out.print(str + cellArray[j] + "\").append(\" " + courseName + "<br>\").css(\"background-color\", \"green\");");
                                out.print(str + cellArray2[j] + "\").append(\" " + courseName + "<br>\").css(\"background-color\", \"green\");");
                                out.print(str + cellArray3[j] + "\").append(\" " + courseName + "<br>\").css(\"background-color\", \"green\");");

                                System.out.print("cellArray"+cellArray[j]); 
                             }
                             out.print("</script>");
             }
                             
                        }
                    }
            
        %>
        
          <%    
                   
                                    
                sessionFactory = new Configuration().configure().buildSessionFactory();
                openSession = sessionFactory.openSession();
                transaction = null;
                String query_sql_Desired = "from ScheduleBlock S where S.id.emailAddress = '" + email + "' AND  S.id.year = '"+year+"' AND S.id.role = 'Desired' ";
                String querySchool_Desired = "from AcademicYear A, Student S where S.emailAddress = '" + email + "' AND  A.id.year = '"+year+"' AND S.schoolCode = A.id.schoolCode";
                String querySection_Desired = "from Section";
                Query sectionQuery_Desired = openSession.createQuery(querySection_Desired);
                Query querysql_Desired = openSession.createQuery(query_sql_Desired);
                Query query_school_Desired = openSession.createQuery(querySchool_Desired);
                List l_Desired = query_school_Desired.list();
                List schedule_Desired = querysql_Desired.list();
                List section_Desired = sectionQuery_Desired.list();
                int scheduleYear_Desired;
                String semester_Desired ="";
                String timeSlot_Desired = "";
                int sectionId_Desired;
                 String courseName_Desired = "";
                  String courseCode_Desired = "";
                  int sectionNumber_Desired;
                  String scheduleBlock_Desired = "";
                for(int i=0; i<schedule_Desired.size(); i++){          
                    ScheduleBlock s = (ScheduleBlock)schedule_Desired.get(i);
                        scheduleYear_Desired = s.getId().getYear();
                        semester_Desired = s.getId().getSemester();
                         timeSlot_Desired = s.getId().getTimeSlot();
                         sectionId_Desired = s.getId().getSectionId();
                    for(int k=0; k<section_Desired.size(); k++){
                        Section sec = (Section)section.get(k);
                        if(sectionId_Desired == sec.getId()){
                             courseName_Desired = sec.getCourseName();
                             courseCode_Desired = sec.getCourseCode();
                             sectionNumber_Desired = sec.getSectionNumber();
                            // scheduleBlock = sec.getScheduleBlock();
                             String[] splitScheduleBlock = timeSlot_Desired.split(":");
                             String period = splitScheduleBlock[0];
                             String numDays = splitScheduleBlock[1];
                             String[] daysArray = numDays.split(";");
                             System.out.print("Days array"+daysArray.length);
                              System.out.print("period "+period);
                               System.out.print("numDays"+daysArray); 
                
                          
                       out.print("<script>");
                            for(int j=0;j<daysArray.length; j++)
                             {
                                String[] cellArray = new String[daysArray.length];
                                 String[] cellArray2 = new String[daysArray.length];
                                  String[] cellArray3 = new String[daysArray.length];
                            
                                 String str = "$(\"#";
                               cellArray[j] = "d" + period + daysArray[j];
                                cellArray2[j] = "d"+ period + (Integer.parseInt(daysArray[j]) +numCycle);
                                cellArray3[j] = "d"+ period + (Integer.parseInt(daysArray[j]) +numCycle+numCycle);
                                out.print(str + cellArray[j] + "\").append(\" " + courseName_Desired + "<br>\").css(\"background-color\", \"green\");");
                                out.print(str + cellArray2[j] + "\").append(\" " + courseName_Desired + "<br>\").css(\"background-color\", \"green\");");
                                out.print(str + cellArray3[j] + "\").append(\" " + courseName_Desired+ "<br>\").css(\"background-color\", \"green\");");

                                System.out.print("cellArray"+cellArray[j]); 
                             }
                             out.print("</script>");
             }
                             
                        }
                    }
            
        %>
        
                
        <% 
                sessionFactory = new Configuration().configure().buildSessionFactory();
                 openSession = sessionFactory.openSession();
                 transaction = null;
                String query_sql_ScheduleBlock = "from ScheduleBlock S where S.id.emailAddress = '" + email + "' AND  S.id.year = '" + year + "' AND S.id.role = 'Assigned' ";
                String querySchool_Academic = "from AcademicYear A, Student S where S.emailAddress = '" + email + "' AND  A.id.year = '" + year + "' AND S.schoolCode = A.id.schoolCode";
                String querySection_sql = "from Section";
                Query sectionQuery_sql = openSession.createQuery(querySection_sql);
                Query queryhql = openSession.createQuery(query_sql_ScheduleBlock);
                Query query_school_sql = openSession.createQuery(querySchool_Academic);
                
                List schedule_result = queryhql.list();
                List section_result = sectionQuery_sql.list();
        
                ArrayList<Object[]> academicList_result
                    = (ArrayList<Object[]>) query_school_sql.list();

            AcademicYear academic_reslut = new AcademicYear();
            int num = 0;
            //Displaying the result of the DB in the Request Tag on the admin's page
            for (Object[]  item : academicList_result) {
                academic_reslut = (AcademicYear)item[0];
                num= Integer.parseInt(academic_reslut.getScheduleCycle());
                
            }

            String hqlFriends = "from Friends F where (F.userEmail = "
                    + "'"+email+"') OR ( F.friendEmail='"+email+"')";
            
            Query friendQuery = openSession.createQuery(hqlFriends);
            ArrayList<Friends> friendsList = 
                    (ArrayList<Friends>)friendQuery.list();
            ArrayList<String> friendsCourseArray = new ArrayList<String>();
            //Displaying the result of the DB in the Request Tag on the admin's page
            for (Friends friend : friendsList) {
                String eMail = friend.getFriendEmail();
                String myEmail = friend.getUserEmail();
                if(!eMail.equals(email))
                    friendsCourseArray.add(eMail);
                 else
                    if(!myEmail.equals(email))
                         friendsCourseArray.add(myEmail);
                
                String hqlSchedule = "from ScheduleBlock";
                Query scheduleQuery = openSession.createQuery(hqlSchedule);
                List friendSchedule = scheduleQuery.list();
                for(int i=0; i<friendSchedule.size(); i++){
                    ScheduleBlock block = (ScheduleBlock)friendSchedule.get(i);
                    for(int j=0; j<friendsCourseArray.size(); j++){
                        String scheduleEmail = friendsCourseArray.get(j);
                        String name =(String) PlannerMethods.getUserList().get(scheduleEmail);
                        if(scheduleEmail.equals(block.getId().getEmailAddress()))
                        {
                            for(int m =0; m<schedule_result.size();m++){
                                 ScheduleBlock sch = (ScheduleBlock) schedule_result.get(m);
                                 if(sch.getId().getSectionId()== block.getId().getSectionId() && block.getId().getRole().equals("Assigned")){
                               
                                   String time = sch.getId().getTimeSlot();
                             String[] splitScheduleBlock = time.split(":");
                            String period = splitScheduleBlock[0];
                            String numDays = splitScheduleBlock[1];
                            String[] daysArray = numDays.split(";");
                            System.out.print("Days array" + daysArray.length);
                            System.out.print("period " + period);
                            System.out.print("numDays" + daysArray);

                            out.print("<script>");
                            for (int x = 0; x < daysArray.length; x++) {
                                String[] cellArray = new String[daysArray.length];
                                 String[] cellArray2 = new String[daysArray.length];
                                  String[] cellArray3 = new String[daysArray.length];
                                cellArray[x] = "f" + period + daysArray[x];
                                cellArray2[x] = "f"+ period + (Integer.parseInt(daysArray[x]) +num);
                                cellArray3[x] = "f"+ period + (Integer.parseInt(daysArray[x]) +num+num);
                                String str = "$(\"#";
                                for(int y =0; y<section_result.size(); y++)
                                {
                                    Section s = (Section)section_result.get(y);
                                    
                                    if(s.getId()== sch.getId().getSectionId())
                                   out.print(str + cellArray[x] + "\").append(\" " + s.getCourseName() + "<br/>  "+ name + "<br>\").css(\"background-color\", \"blue\");");

                                }
                               
                                System.out.print(scheduleEmail);
                                
                                System.out.print("cellArray" + cellArray[x]);
                                System.out.print("cellArray2" + cellArray2[x]);
                                System.out.print("cellArray3" + cellArray3[x]);
                            }
                                  out.print("</script>");
                          
                                 }
                            }
                        }
                    }
                
                    
                }
            }
        
        
            %>
        
    </body>

    <script>        
        $(function() {
            var unselectedValues = [];
          var selectedValues = [];
          var splitCourseInfo;
          var rawCourseCode;
         <sec:CourseOptionHandler userEmail="${userName}">
            var choice = "${desiredCourseElement}";
            var i = "${multiSelectIndex}"
            splitCourseInfo = "${desiredCourseElement}".toString().split("-");
            unselectedValues.push("${desiredCourseElement}");
            console.log(unselectedValues);
             console.log(choice);
             console.log(i);
             $('#desired-course-select').append("<option title = '${instructorName}'>${desiredCourseElement}</option>");
         </sec:CourseOptionHandler>
          
          $('#desired-course-select').multiSelect({
            afterSelect: function(values){
                splitCourseInfo = values.toString().split("-");
                rawCourseCode = splitCourseInfo[0];
                var notInList = 1;
                for(var j = 0; j < selectedValues.length; j++) {
                    
                    if(selectedValues[j].toString().substring(0,6) == rawCourseCode) {
                        notInList = -1;
                        break;
                    }
                    else {
                        notInList = 1;
                      
                    }
                }
                if(notInList > 0) {
                selectedValues.push(values.toString());
                unselectedValues.splice(unselectedValues.indexOf(values.toString()),1);
                //$('#desired-course-select').append("<option>"+rawCourseCode+"</option>");
                 } else {
                     for(var i = 0; i < selectedValues.length; i++) {
                         if(selectedValues[i].toString().substring(0,6) == rawCourseCode) {
                             unselectedValues.splice(unselectedValues.indexOf(values.toString()),1);
                             unselectedValues.push(selectedValues.splice(i,1).toString());
                             selectedValues.push(values.toString());
                             i = selectedValues.length;
                         }
                     }
                 }
                 //ITERATES THROUGH SELECTED LIST
                 for(i = 0; i < selectedValues.length; i++){
                     var name = selectedValues[i].toString();
                    $('#desired-course-select').find('option:contains('+name+')').attr("selected",true);
                    $('#desired-course-select').multiSelect('refresh');
                }
                //ITERATES THROUGH UNSELECTED LIST
                 for(i = 0; i < unselectedValues.length; i++){
                     var name = unselectedValues[i].toString();
                     $('#desired-course-select').find('option:contains('+name+')').attr("selected",false);
                    $('#desired-course-select').multiSelect('refresh');
                }
                 
                console.log("select method");
                console.log("selected values: "); console.log(selectedValues);
                console.log("unselected values: "); console.log(unselectedValues);
                //alert("Select value: "+values);
            },
            afterDeselect: function(values){

                selectedValues.splice(selectedValues.indexOf(values.toString()),1);
                unselectedValues.push(values.toString());
//                var i;
//                $('#desired-course-select').remove("<option>"+rawCourseCode+"</option>");
//                for(i = 0; i < selectedValues.length; i++) {
//                    $('#desired-course-select').append("<option>"+rawCourseCode+"</option>");
//                }
//                
                                 //ITERATES THROUGH SELECTED LIST
                 for(i = 0; i < selectedValues.length; i++){
                     var name = selectedValues[i].toString();
                    $('#desired-course-select').find('option:contains('+name+')').attr("selected",true);
                    $('#desired-course-select').multiSelect('refresh');
                }
                //ITERATES THROUGH UNSELECTED LIST
                 for(i = 0; i < unselectedValues.length; i++){
                     var name = unselectedValues[i].toString();
                     $('#desired-course-select').find('option:contains('+name+')').attr("selected",false);
                    $('#desired-course-select').multiSelect('refresh');
                }
                 
                 
                console.log("deselect method");
                console.log("selected values: "); console.log(selectedValues);
                console.log("unselected values: "); console.log(unselectedValues);
                //alert("Deselect value: "+values);
            }
        });
         
        
          $('#desired-course-select').multiSelect({});         
         
           });
        $(document).on('hidden.bs.modal', '.modal', function() {
            $('.modal:visible').length && $(document.body).addClass('modal-open');
        });
                
       /* function showScheduleBlock(str) {
            var xmlhttp;
            if (str == "") {
                document.getElementById("scheduleBlock").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    //document.getElementById("scheduleBlock").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "ScheduleBlockServlet?q=" + str, true);
            xmlhttp.send();

        }
        */
        function showScheduleBlockNew() {
            var xmlhttp;
            var str = $("#academicYr").val();
                     $("#academic").val(str);
              
              var coursecode = $("#courseCode").val();
                  $("#course").val(coursecode);
              
            if (str == "") {
                document.getElementById("addScheduleBlock").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("addScheduleBlock").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "ScheduleBlockServlet?q=" + str, true);
            xmlhttp.send();

        }
    
    
        function showSemesters(str) {
            var xmlhttp;
            if (str == "") {
                $("#semester").html("");
                return;
            }
            //showScheduleBlock(str);
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    $("#semester").html(xmlhttp.responseText);
                }
            }
            xmlhttp.open("GET", "SemesterServlet?q=" + str, true);
            xmlhttp.send();

        }
        
          function appendSection(){
             var numOptions = $('#courseSection option').size(); 
             $("#courseSection").append("<option>"+numOptions+"</option>");
             }

        var map = new Map();
        var blockMap = new Map();
        

        function checkCourse(str) {
            var courseExists = false;
            var courseExistsPreviousYear = false;
            var courseCode = str;
            var code = $('#academicYr').val();
            var info = "";
            
            
            var xmlhttp;
            if (str == "") {
                document.getElementById("semester").innerHTML = "";
                return;
            }
            //showScheduleBlock(str);
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function() {

                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    info = xmlhttp.responseText;
                    var parsedInfo = info.split("-");
                    //Course info variables
                    //Name - section - instructor - semester - schedule block
                    var sections = parsedInfo[1].split(","); //array of sections
                    var instructors = parsedInfo[2].split(",");//array of instructors
                    var name = parsedInfo[0];
                    console.log("Name: " + name);
                    if(name != ""){
                        if(instructors != "")
                            courseExists = true;
                        else
                            courseExistsPreviousYear = true;
                    }
                    
                    
                    // map instructors to each sections
                    for(i = 0; i < sections.length; i++){
                        map.set(sections[i],instructors[i]);
                    }
                    var semesters = parsedInfo[3].split(",");
                    var scheduleBlocks = parsedInfo[4].split("|");                    
                    
                    for(i = 0; i< scheduleBlocks.length; i++){
                        blockMap.set(sections[i],scheduleBlocks[i]);
                     }
                    
                    //Set input values
                    var button = "<button data-toggle=\"modal\" data-target=\"#add-course\" type=\"button\" class=\"btn btn-default\" id=\"add-course-button\">Add <span class=\"glyphicon glyphicon-plus\"></span></button>" 
                    var addSection = "<button onclick = \"appendSection()\" type=\"button\" class=\"btn btn-default\" id=\"add-section-button\">Add New Section/Professor<span class=\"glyphicon glyphicon-plus\"></span></button>" 
                    if(courseExists){                        
                        $('#noCourse').html("");
                        $('#courseName').val(name).attr("readonly",true);
                        $('#addSection').html(addSection);
                    }
                    else if(courseExistsPreviousYear){
                        $('#noCourse').html("<h6>This course does not exist for this academic year. Press 'add' to enter the course.<h6>" + button);
                        $('#addCourseName').val(name).attr("readonly",true);
                    }
                    else
                        $('#noCourse').html("<h6>This course does not exist. Press 'add' to enter the course.<h6>" + button);
                    
                    //Check if options exist in select, 
                    //Create them if they do not.
                    for(i = 0; i < sections.length; i++){
                        var section = sections[i];
                        if(!$("#courseSection option[value='"+section+"']").length > 0 && section != ""){
                             $("#courseSection").append("<option>"+section+"</option>");
                        }
                        
                        
                    }
                    
                   
                    for(i = 0; i < semesters.length; i++){
                        var semester = semesters[i];
                        console.log("semester : " + semester);
                         console.log("semester i value : " + (i+1));
                        if(!$("#semester option[value='"+(i+1)+"']").length > 0 && semester != ""){
                             $("#semester").val((i+1));
                             $("#semester").append("<option>"+semester+"</option>");
                             
                        }
                        
                        
                    }
                   //if(!$("#semesterRange option[value='"+semester+"']").length > 0){
                      //   $("#semesterRange").append("<option>"+semester+"</option>");
                    //}
         
                  //  if(!$("#scheduleBlock option[value='"+scheduleBlock+"']").length > 0){
                  //     $("#scheduleBlock").append("<option>"+scheduleBlock+"</option>"); 
                  //  }
                    
                    
                    //$('#semesterRange').find('option:contains('+semester+')').attr("selected",true);
                    //$('#scheduleBlock').find('option:contains('+scheduleBlock+')').attr("selected",true);
                }
            }
            xmlhttp.open("GET", "SemesterVerificationServlet?code=" + code + "&courseCode=" + courseCode, true);
            console.log();            xmlhttp.send();
           
        }
        function displayScheduleBlock(block){
            var temp = block.split(";");
            var period = block[0];
            var dayArray = temp[1].split(",");
            var cellArray = [];
            var prefix;
            
            if ((($("#assigned").data('bs.modal') || {}).isShown))
                prefix = "a";
            else
                prefix = "d";
            
            console.log(($("#assigned").data('bs.modal') || {}).isShown);
            
        
    }
        //----Related to editing schedule----
         function changeInstructor(){            
            var choice = ($("#courseSection option:selected" ).text());
            var block = blockMap.get(choice);
            $('#courseInstructor').val(map.get(choice));
            $("#scheduleBlock").val(blockMap.get(choice));
                       
            console.log( "the schedule value is :"+$("#scheduleBlock").val())
            if(courseExists)
                $('#courseInstructor').attr("readonly",true);
            
            if(choice == "Select Section")
                $('#courseInstructor').attr("readonly",false);
        }
        
        function changeScheduleBlock(){
            var choice = ($("#scheduleBlock option:selected" ).text());
            console.log(choice);
            $('#scheduleBlockHidden').val(choice);
        }
        
        //clears edit schedule fields when course name changes
        function clearData(){
                $('#courseInstructor').val("").attr("readonly",false);
                $("#courseSection").empty().append("<option>Select Section</option>");
        }
        function unfriend(email){
                   var fromEmail = $("#unfriend").val();
                    console.log(fromEmail);
            window.location.replace("UnFriendServlet?to="+email+"&from="+fromEmail);
                  
        }

         function deleteRequest(email){
             window.location.replace("DeleteFriendRequestServlet"+email);
                    
            }
            function approveRequest(email){
             window.location.replace("ApproveRequestServlet"+email);
                    
            }

        //-------INITIALIZE SCHEDULE-------
        var assignedSchedule = document.getElementById("assigned-schedule").innerHTML;
        var desiredSchedule = document.getElementById("desired-schedule").innerHTML;
        //do this stuff on load:
        $(function() {
            if ($("#assigned-body").html() == "") {
                $("#assigned-body").html(assignedSchedule);
            }

            if ($("#desired-body").html() == "") {
                $("#desired-body").html(desiredSchedule);
            }
        });

        //----SCHEDULE MODAL CONTROLS (Toggle, edit, export)----
        var withFriends = false; //store current state of schedule
        var friendSched = $("#friends-schedule").html();

        $("#a-toggle").click(function() {
            if (!withFriends) {
                $("#assigned-title").html("Assigned Schedule With Friends");
                $("#assigned-body").html(friendSched);
                withFriends = true;
            }
            else {
                $("#assigned-body").html(assignedSchedule);
                $("#assigned-title").html("Assigned Schedule");
                withFriends = false;
            }
        });

        //----EXPORT FUNCTIONALITY----
        var fileName;
        var file;
        var assigned; //true if assigned is being exported

        $("#a-export").click(function() {
            assigned = true;
        });
        $("#d-export").click(function() {
            assigned = false;
        });

        $("#download").click(function() {

            fileName = $("#filename").val() + ".html";
            if (assigned) {
                file = "<h4>" + $("#assigned-title").html() + "</h4>" + $("#assigned-body").html();
            }
            else {
                file = "<h4>" + $("#desired-title").html() + "</h4>" + $("#desired-body").html();
            }

            if ($("#filename").val() == "")
                alert('Please enter a file name.');
            else
                download(fileName, file);
        });


        //FOR EXPORTING FILES
        function download(filename, text) {
            var pom = document.createElement('a');
            pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
            pom.setAttribute('download', filename);

            if (document.createEvent) {
                var event = document.createEvent('MouseEvents');
                event.initEvent('click', true, true);
                pom.dispatchEvent(event);
            }
            else {
                pom.click();
            }

        }


    </script>
   


</html>
