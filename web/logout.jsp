<%-- 
    Document   : logout
    Created on : Apr 2, 2015, 2:57:35 AM
    Author     : Crazzykid
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Logout Page</title>
    </head>
    
    <%
        HttpSession newsession = request.getSession(false);
          if (newsession != null) 
            {
                newsession.invalidate();
            }
           response.sendRedirect("index.jsp");
   %>
    <body>
        
    </body>
</html>
