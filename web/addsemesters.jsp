<%-- 
    Document   : addsemesters
    Created on : Apr 22, 2015, 8:35:26 PM
    Author     : ShawnCruz
--%>

<%@page import="cse308.School"%>
<%@page import="java.util.List"%>
<%@page import="org.hibernate.Transaction"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="cse308.Student"%>
<%@page import="cse308.User"%>
<%@page import="java.util.ArrayList"%>
<%@page import="org.hibernate.cfg.Configuration"%>
<%@page import="org.hibernate.Query"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Add Semesters Page</title>
        <link rel='stylesheet' href='http://codepen.io/assets/libs/fullpage/jquery-ui.css'>
        <link rel="stylesheet" href="css/reg_style.css">
        <script type="text/javascript" src="./js/jquery.js"></script>
        <script type='text/javascript' src='./js/jquery.tipsy.js'></script>
        <link rel="stylesheet" href="./css/tipsy.css" type="text/css"/>
    </head>
    
   

    <body>
        <%
            int numSemesters = Integer.parseInt(request.getParameter("numSem"));
            String school = request.getParameter("schoolselect");
            String accYear = request.getParameter("accYear");
        %>
       <div class=\"register-card\">
       <form action="AddAcademicYearServlet">
        <%
            for(int i = 0; i < numSemesters; i++)
        out.println("<div class=\"register-card\">"
           +" <h1>Semester "+(i+1) +"</h1><br>"
            +    "<input type=\"text\" rel=\"tipsy\" name=\"numPer\" id=\"numPer\" placeholder=\"No. Periods (In Each Day)\" onblur=\"verifyNumPer()\" title = \"This field is required. Please enter the number of periods\">"
            +    "<input type=\"text\" rel=\"tipsy\" name=\"numDays\" id=\"numDays\" placeholder=\"No. Days (In Schedule)\" onblur=\"verifyNumDays()\" title = \"This field is required. Please enter the number of days\">"
            +    "<input type=\"text\" rel=\"tipsy\" name=\"rangeLunc\" id=\"rangeLunc\" placeholder=\"Lunch Period Range\" onblur=\"verifyRangeLunc()\" title = \"This field is required. Please enter the range of periods for lunch\">"
            +    "<input type=\"text\" rel=\"tipsy\" name=\"scheduleBlock\" id=\"scheduleBlock\" placeholder=\"Schedule Block\"  title = \"This field is required. Please enter the valid schedule blocks\">"
            +    "<br><br>"
            + " </div>");
        %>
         <input type="hidden" name="accYear" value="<% out.print(accYear); %>" >
         <input type="hidden" name="numSem" value="<% out.print(numSemesters); %>" >
        <input type="hidden" name="schoolselect" value="<% out.print(school); %>" >
        <input type="submit" name="register" class="register register-submit" value="Submit">
        </form>
        </div>
<!--        <script src='http://codepen.io/assets/libs/fullpage/jquery_and_jqueryui.js'></script>-->
        <script type='text/javascript'>
            $(function() {
                $('select[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
                $('input[rel=tipsy]').tipsy({gravity: 'w', trigger: 'manual'});
                });
            
            var schoolSelect = document.getElementById("schoolSelect");
            var numberSemesters = document.getElementById("numSem");
            var numberDays = document.getElementById("numDays");
            var numberPer = document.getElementById("numPer");
            var rangeLunch = document.getElementById("rangeLunc");
            var legalBlocks = document.getElementById("legalBlocks");
            var academicYear = document.getElementById("acdYr");
            
            function verifySchool(){
                if(schoolSelect.value=="None")
                    $("#schoolSelect").tipsy("show");
                else
                     $("#schoolSelect").tipsy("hide");
            }
            function verifyNumSemesters() {
                if (numberSemesters.value == "") {
                    $("#numSem").tipsy("show");
                }
                else
                    $("#numSem").tipsy("hide");
            }
            function verifyNumDays() {
                if (numberDays.value == "")
                    $("#numDays").tipsy("show");
                else
                    $("#numDays").tipsy("hide");
            }
            function verifyNumPer() {
               if (numberPer.value == "")
                    $("#numPer").tipsy("show");
                else
                    $("#numPer").tipsy("hide");
            }
            function verifyRangeLunc() {
                if (rangeLunch.value == "")
                    $("#rangeLunc").tipsy("show");
                else {
                    $("#rangeLunc").tipsy("hide");
                }
            }
            function verifyLegalBlocks() {
                if (legalBlocks.value == "")
                    $("#legalBlocks").tipsy("show");
                else {
                    $("#legalBlocks").tipsy("hide");
                }
            }
            function verifyAcdYr() {
                if (academicYear.value == "")
                    $("#acdYr").tipsy("show");
                else {
                    $("#acdYr").tipsy("hide");
                }
            }
        </script>
    </body>
</html>

